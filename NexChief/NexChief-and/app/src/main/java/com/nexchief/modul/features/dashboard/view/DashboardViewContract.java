package com.nexchief.modul.features.dashboard.view;

public interface DashboardViewContract {

    void getData(int countPrincipal, int countDistributor, int countUser);

    void countPrincipal(int countPrincipal);

    void countDistributor(int countDistributor);

    void getLastUpdated(String lastUpdated);

    void countUser(int countUser);

    void onFailed(String message);


}
