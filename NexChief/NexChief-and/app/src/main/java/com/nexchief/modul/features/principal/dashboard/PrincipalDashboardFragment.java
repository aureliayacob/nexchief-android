package com.nexchief.modul.features.principal.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nexchief.R;
import com.nexchief.databinding.FragmentDashboardPrincipalBinding;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.principal.add.AddPrincipalActivity;
import com.nexchief.modul.features.principal.dashboard.presenter.PrincipalDashboardPresenter;
import com.nexchief.modul.features.principal.dashboard.view.PrincipalDashboardViewContract;
import com.nexchief.util.adapter.PrincipalAdapter;

import java.util.List;

public class PrincipalDashboardFragment extends Fragment implements PrincipalDashboardViewContract {

    private FragmentDashboardPrincipalBinding binding;
    private PrincipalDashboardPresenter presenter;

    private RecyclerView recyclerView;
    private PrincipalAdapter principalAdapter;

    private SearchView searchView;
    private NestedScrollView nestedScrollView;
    private int size = 7, page = 0, totalPage = 0;
    private ProgressBar progressBar;
    private TextView tvEmptyData;


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.feature_search, menu);

        MenuItem menuItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) menuItem.getActionView();
        super.onCreateOptionsMenu(menu, inflater);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            //TODO -- search by API -- DONE
            @Override
            public boolean onQueryTextSubmit(String query) {
                //ketika user klik submit baru ditampilkan
                presenter.searchPrincipal(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //ketika user ngetik setiap char
                presenter.searchPrincipal(newText);
                return true;
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentDashboardPrincipalBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        setHasOptionsMenu(true);
        recyclerView = binding.recyclerView;
        presenter = new PrincipalDashboardPresenter(this);
        nestedScrollView = binding.scrollViewPrincipal;
        progressBar = binding.progressBar;
        tvEmptyData = binding.tvEmptyData;
        //first page/default page
        presenter.getPrincipal(size, page);

        binding.fabAddPrinicpals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddPrincipalActivity.class);
                startActivity(intent);
            }
        });

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                //check condition
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight() && page <= totalPage) {
                    // when reaching last item position
                    //increase page
                    page++;
                    //call method
                    presenter.getPrincipal(size, page);
                }
            }
        });
        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        //back to first page
        page = 0;
        presenter.getPrincipal(size, page);
    }

    @Override
    public void getData(List<Principal> principalList) {
        progressBar.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        principalAdapter = new PrincipalAdapter(principalList, this.getContext());
        recyclerView.setAdapter(principalAdapter);
    }

    public void updatePrincipalList(List<Principal> principals) {
        principalAdapter.addToList(principals);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void getTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    @Override
    public void getSearchResult(List<Principal> principals) {
        principalAdapter.updateFilteredList(principals);
    }

    @Override
    public void onFailure(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyDataHelper() {
        tvEmptyData.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideEmptyDataHelper() {
        tvEmptyData.setVisibility(View.INVISIBLE);

    }


}
