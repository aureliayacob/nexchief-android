package com.nexchief.modul.features.principal.add.presenter;

import android.content.SharedPreferences;
import android.util.Patterns;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.principal.add.view.AddPrincipalViewContract;
import com.nexchief.util.retrofit.services.PrincipalService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPrincipalPresenter implements AddPrincipalPresenterContract {

    private final AddPrincipalViewContract view;
    private final PrincipalService principalService;

    public AddPrincipalPresenter(AddPrincipalViewContract view) {
        this.view = view;
        this.principalService = ServiceGenerator.createService(PrincipalService.class);
    }

    public void saveData(TextInputEditText textPrincipalId, TextInputLayout principalIdLyt,
                         TextInputEditText textName, TextInputEditText textAddress,
                         TextInputEditText textCity, TextInputEditText textEmail,
                         TextInputEditText textPhone, TextInputEditText textLicensed,
                         TextInputLayout textNameLyt, TextInputLayout textAddressLyt,
                         TextInputLayout textCityLyt, TextInputLayout textEmailLyt,
                         TextInputLayout textPhoneLyt, TextInputLayout textLicensedLyt,
                         Principal principal, SharedPreferences sharedPreferences) {

        if (validate(textPrincipalId, principalIdLyt, textName, textAddress, textCity,
                textEmail, textPhone, textLicensed, textNameLyt, textAddressLyt, textCityLyt,
                textEmailLyt, textPhoneLyt, textLicensedLyt)) {
            principal.setCreatedBy(sharedPreferences.getString("username", ""));
            principal.setUpdatedBy(sharedPreferences.getString("username", ""));
            principalService.addNewPrincipal(principal).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        view.onSuccess();
                    } else {
                        JSONObject errorObject;
                        try {
                            errorObject = new JSONObject(response.errorBody().string());
                            view.onFailed(errorObject.getString("message"));
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });

        } else {
            view.onFailed("Invalid Input");
        }


    }

    @Override
    public boolean anyChanges(TextInputEditText principalId, TextInputEditText name,
                              TextInputEditText address, TextInputEditText city,
                              TextInputEditText email, TextInputEditText phone,
                              TextInputEditText fax) {
        return !principalId.getText().toString().isEmpty() ||
                !name.getText().toString().isEmpty() ||
                !address.getText().toString().isEmpty() ||
                !city.getText().toString().isEmpty() ||
                !email.getText().toString().isEmpty() ||
                !phone.getText().toString().isEmpty() ||
                !fax.getText().toString().isEmpty();
    }

    public boolean validate(TextInputEditText textPrincipalId, TextInputLayout principalIdLyt,
                            TextInputEditText textName, TextInputEditText textAddress,
                            TextInputEditText textCity, TextInputEditText textEmail,
                            TextInputEditText textPhone, TextInputEditText textLicensed,
                            TextInputLayout textNameLyt, TextInputLayout textAddressLyt,
                            TextInputLayout textCityLyt, TextInputLayout textEmailLyt,
                            TextInputLayout textPhoneLyt, TextInputLayout textLicensedLyt) {

        if (!textPrincipalId.getText().toString().isEmpty() &&
                !Pattern.compile("^([A-Z]{5,12}|[A-Z0-9]{5,12})$")
                        .matcher(textPrincipalId.getText().toString())
                        .matches()) {
            principalIdLyt.setErrorEnabled(true);
            principalIdLyt.setError("ID Format Invalid, ID must contain ID must contain 5 to 12 letters or number");
        } else {
            principalIdLyt.setErrorEnabled(false);
        }

        if (textName.getText().toString().isEmpty()) {
            textNameLyt.setErrorEnabled(true);
            textNameLyt.setError("* Name is Required");
            return false;
        } else {
            textNameLyt.setErrorEnabled(false);
        }

        if (textAddress.getText().toString().isEmpty()) {
            textAddressLyt.setErrorEnabled(true);
            textAddressLyt.setError("* Address is Required");
            return false;
        } else {
            textAddressLyt.setErrorEnabled(false);
        }

        if (textAddress.getText().toString().length() > 100) {
            textAddressLyt.setErrorEnabled(true);
            textAddressLyt.setError("* Address must less than 100 characters");
            return false;
        } else {
            textAddressLyt.setErrorEnabled(false);
        }

        if (textCity.getText().toString().isEmpty()) {
            textCityLyt.setErrorEnabled(true);
            textCityLyt.setError("* City is Required");
            return false;
        } else {
            textCityLyt.setErrorEnabled(false);
        }

        if (textEmail.getText().toString().isEmpty()) {
            textEmailLyt.setErrorEnabled(true);
            textEmailLyt.setError("* Email is Required");
            return false;
        } else {
            textEmailLyt.setErrorEnabled(false);
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(textEmail.getText().toString()).matches()) {
            textEmailLyt.setErrorEnabled(true);
            textEmailLyt.setError("* Email format is invalid");
            return false;
        } else {
            textEmailLyt.setErrorEnabled(false);
        }

        if (textPhone.getText().toString().isEmpty()) {
            textPhoneLyt.setErrorEnabled(true);
            textPhoneLyt.setError("* Phone is Required");
            return false;
        } else {
            textPhoneLyt.setErrorEnabled(false);
        }

        if (textLicensed.getText().toString().equalsIgnoreCase("Select Date")) {
            textLicensedLyt.setErrorEnabled(true);
            textLicensedLyt.setError("* Select Date");
            return false;
        } else {
            textLicensedLyt.setErrorEnabled(false);
        }
        return true;
    }
}
