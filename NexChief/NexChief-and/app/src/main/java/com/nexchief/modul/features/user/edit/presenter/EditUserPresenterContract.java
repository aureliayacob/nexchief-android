package com.nexchief.modul.features.user.edit.presenter;

import android.content.Intent;
import android.content.SharedPreferences;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.model.User;

import java.util.List;

public interface EditUserPresenterContract {

    void getData(Intent intent);

    String getDistributor(User user);

    void getPrincipals();

    void getDistributors(int id);

    Principal getPrincipalByString(String string);

    Distributor getDistributorByString(String string);

    void editUser(TextInputEditText email, TextInputLayout emailLyt,
                  TextInputEditText userId, TextInputEditText name,
                  TextInputEditText address, TextInputEditText phone,
                  TextInputLayout userIdLyt, TextInputLayout nameLyt,
                  TextInputLayout addressLyt, TextInputLayout phoneLyt, TextInputLayout principalLyt,
                  User user, SharedPreferences sharedPreferences);

    void isDistributorActive(TextInputLayout distributorLyt, User user);


}
