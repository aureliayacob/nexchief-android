package com.nexchief.modul.features.user.dashboard.presenter;

public interface UserDashboardPresenterContract {
    void getUser(int size, int page);

    void searchUser(String keyword);
}
