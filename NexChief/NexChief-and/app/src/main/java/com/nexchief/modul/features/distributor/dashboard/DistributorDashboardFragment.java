package com.nexchief.modul.features.distributor.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nexchief.R;
import com.nexchief.databinding.FragmentDashboardDistributorBinding;
import com.nexchief.model.Distributor;
import com.nexchief.modul.features.distributor.add.AddDistributorActivity;
import com.nexchief.modul.features.distributor.dashboard.presenter.DistributorPresenter;
import com.nexchief.modul.features.distributor.dashboard.view.DistributorDashboardViewContract;
import com.nexchief.util.adapter.DistributorAdapter;

import java.util.List;

public class DistributorDashboardFragment extends Fragment implements DistributorDashboardViewContract {

    private FragmentDashboardDistributorBinding binding;
    private DistributorPresenter presenter;

    private RecyclerView recyclerView;
    private DistributorAdapter distributorAdapter;

    private SearchView searchView;
    private NestedScrollView nestedScrollView;
    private int size = 7, page = 0, totalPage = 0;
    private ProgressBar progressBar;
    private TextView tvEmptyData;


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.feature_search, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        searchView = (SearchView) item.getActionView();
        super.onCreateOptionsMenu(menu, inflater);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.searchDistributor(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                presenter.searchDistributor(newText);
//                distributorAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentDashboardDistributorBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        setHasOptionsMenu(true);
        recyclerView = binding.recyclerViewDistributor;

        presenter = new DistributorPresenter(this);
        nestedScrollView = binding.scrollViewDistributor;
        progressBar = binding.progressBar;
        tvEmptyData = binding.tvEmptyData;

        //first page/default page
        presenter.getDistributor(size, page);

        binding.fabAddDistributors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddDistributorActivity.class);
                startActivity(intent);
            }
        });

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight() && page <= totalPage) {
                    page++;
                    presenter.getDistributor(size, page);
                }
            }
        });
        return root;
    }

    @Override
    public void updateDistributorList(List<Distributor> distributorList) {
        distributorAdapter.addToList(distributorList);
    }

    @Override
    public void updateFilteredList(List<Distributor> distributorList) {
        distributorAdapter.updateFilteredList(distributorList);
    }

    @Override
    public void showAllDistributor(List<Distributor> distributorList) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        distributorAdapter = new DistributorAdapter(distributorList, this.getContext());
        recyclerView.setAdapter(distributorAdapter);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess() {
    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getTotalPage(int totalPage) {
        this.totalPage = totalPage;

    }

    @Override
    public void showEmptyDataHelper() {
        tvEmptyData.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyDataHelper() {
        tvEmptyData.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResume() {
        this.page = 0;
        presenter.getDistributor(size, page);
        super.onResume();
    }


}
