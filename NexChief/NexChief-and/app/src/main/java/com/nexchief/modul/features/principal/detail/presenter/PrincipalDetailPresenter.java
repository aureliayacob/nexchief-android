package com.nexchief.modul.features.principal.detail.presenter;

import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.principal.detail.view.PrincipalDetailViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.PrincipalService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrincipalDetailPresenter implements PrincipalDetailPresenterContract {

    private final PrincipalDetailViewContract view;
    private final PrincipalService principalService;
    private final DateFormatter dateFormatter;

    public PrincipalDetailPresenter(PrincipalDetailViewContract view) {
        this.view = view;
        this.principalService = ServiceGenerator.createService(PrincipalService.class);
        this.dateFormatter = new DateFormatter();
    }

    @Override
    public void getData(Intent intent) {
        Principal principal = (Principal) intent.getSerializableExtra("principal");
        principalService.getPrincipalById(principal.getPrincipalId()).enqueue(new Callback<ResponseBody>() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(response.body().string());
                        JSONObject tempPrincipal = jsonObject.getJSONObject("data");
                        Principal principalData = new Principal();
                        //set data to model
                        principalData.setPrincipalId(tempPrincipal.getString("principalId"));
                        principalData.setAddress(tempPrincipal.getString("address"));
                        principalData.setName(tempPrincipal.getString("name"));
                        principalData.setCountry(tempPrincipal.getString("country"));
                        principalData.setFax(tempPrincipal.getString("fax"));
                        principalData.setLicensedExpiryDate(dateFormatter.formatStringToDate(tempPrincipal.getString("licensedExpiryDate")));
                        principalData.setPhone(tempPrincipal.getString("phone"));
                        principalData.setEmail(tempPrincipal.getString("email"));
                        principalData.setCity(tempPrincipal.getString("city"));
                        principalData.setPersonId(tempPrincipal.getInt("personId"));
                        principalData.setUpdatedBy(tempPrincipal.getString("updatedBy"));
                        principalData.setCreatedBy(tempPrincipal.getString("createdBy"));
                        principalData.setCreatedAt(dateFormatter.formatStringToDate((tempPrincipal.getString("createdAt"))));
                        principalData.setUpdatedAt(dateFormatter.formatStringToDate((tempPrincipal.getString("updatedAt"))));
                        view.getDetailPrincipal(principalData);
                    } catch (JSONException | IOException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    @Override
    public void deletePrincipal(int id) {

        principalService.deletePrincipal(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.onSuccess("Delete Success");
                } else {
                    view.onFailed("Delete Failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.onFailed("Delete Failed");
            }
        });

    }

    @Override
    public void checkIfHaveDistributor(int id) {
        //check if principal has distributor
        principalService.isHaveDistributor(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        int countDistributor = new JSONObject(response.body().string()).getInt("data");
                        Log.i("HAVE DISTRIBUTOR", "->"+countDistributor);

                        if ( countDistributor >0){
                            //have distributor
                            view.deleteHaveDistributor();

                        }else{
                            view.deleteDontHaveDistributor();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

}
