package com.nexchief.modul.features.distributor.edit.presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.AutoCompleteTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;

public interface EditDistributorPresenterContract {

    void getData(Intent intent);

    void getPrincipals();

    Principal getPrincipalByString(String string);

    void editDistributor(TextInputEditText name, TextInputEditText address, TextInputEditText city,
                         TextInputEditText firstName, TextInputEditText lastName, TextInputEditText phone,
                         TextInputLayout nameLayout, TextInputLayout addressLayout,
                         TextInputLayout cityLayout, TextInputLayout firstNameLayout,
                         TextInputLayout lastNameLayout, TextInputLayout phoneLayout,
                         Distributor distributor, SharedPreferences sharedPreferences);

    void checkPrincipal(String id, TextInputLayout principalLayout, AutoCompleteTextView principal);


}
