package com.nexchief.util.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.nexchief.R;
import com.nexchief.model.Principal;
import com.nexchief.model.User;
import com.nexchief.modul.features.principal.detail.DetailPrincipalActivity;
import com.nexchief.modul.features.user.detail.DetailUserActivity;
import com.nexchief.modul.features.user.edit.EditUserActivity;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> implements Filterable {
    final List<User> allUsers;
    final List<User> filteredUsers;
    final Context context;

    public UserAdapter(List<User> allUsers, Context context) {
        this.allUsers = allUsers;
        this.filteredUsers = new ArrayList<>();
        filteredUsers.addAll(allUsers);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserAdapter.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_user_dashboard, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = filteredUsers.get(position);
        holder.userName.setText(user.getName());
        holder.userId.setText(user.getUserId().toUpperCase());

        holder.itemView.findViewById(R.id.parent_item_layout)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext().getApplicationContext(),
                                DetailUserActivity.class)
                                .putExtra("user", user)
                                .putExtra("todelete", false);
                        context.startActivity(intent);
                    }
                });

        holder.itemView.findViewById(R.id.i_edit_user)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext().getApplicationContext(),
                                EditUserActivity.class)
                                .putExtra("user", user)
                                .putExtra("toedit", user.getId());
                        context.startActivity(intent);
                    }
                });
        holder.itemView.findViewById(R.id.i_delete_user)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext().getApplicationContext(),
                                DetailUserActivity.class)
                                .putExtra("user", user)
                                .putExtra("todelete", true);
                        context.startActivity(intent);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return filteredUsers.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<User> filteredList = new ArrayList<>();

                if (constraint.toString().isEmpty()) {
                    filteredList.addAll(allUsers);
                } else {
                    filteredList.clear();
                    for (User u : allUsers) {
                        String name = u.getName().toLowerCase();
                        if (name.contains(constraint.toString().toLowerCase())) {
                            filteredList.add(u);
                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredUsers.clear();
                filteredUsers.addAll((List<User>) results.values);
                notifyDataSetChanged();

            }
        };
        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView userName;
        private final TextView userId;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.tv_user_name);
            userId = itemView.findViewById(R.id.tv_userid);
        }
    }

    public void addToList(List<User> users) {
        filteredUsers.addAll(users);
        notifyDataSetChanged();
    }


    public void updateFilteredList(List<User> users) {
        filteredUsers.clear();
        filteredUsers.addAll(users);
        notifyDataSetChanged();
    }
}
