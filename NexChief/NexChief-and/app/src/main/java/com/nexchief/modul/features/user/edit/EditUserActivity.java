package com.nexchief.modul.features.user.edit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.databinding.ActivityEditUserBinding;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.model.User;
import com.nexchief.modul.features.user.add.AddUserActivity;
import com.nexchief.modul.features.user.edit.presenter.EditUserPresenter;
import com.nexchief.modul.features.user.edit.view.EditUserViewContract;
import com.nexchief.util.DateFormatter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EditUserActivity extends AppCompatActivity implements EditUserViewContract {

    private EditUserPresenter presenter;
    private ActivityEditUserBinding binding;
    private SharedPreferences sharedPreferences;
    private DateFormatter dateFormatter;

    private List<String> principalString;
    private List<String> status;

    private TextInputLayout userIdLyt, principalLyt, nameLyt, addressLyt,
                            phoneLyt, emailLyt, distriubtorLyt;

    private SwitchCompat disableLogin;
    private Button btnSave;
    private boolean userDisableloginValue = false;

    private ProgressDialog progressDialog;

    private AutoCompleteTextView principalDropdown, distributorDropdown, statusDropDown;

    private TextInputEditText textUserId, textName, textAddress,
            textPhone, textEmail, textRegistrationDate, textUserValidThru,
            textCreatedAt, textCreatedBy, textUpdatedAt, textUpdatedBy;

    private Calendar calendar;
    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String dateToSet = dateFormatter.formatDateToString(calendar.getTime());
            textUserValidThru.setText(dateToSet);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditUserBinding.inflate(getLayoutInflater());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.edit_user);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        View root = binding.getRoot();
        setContentView(root);

        init();
        presenter.getData(getIntent());
        presenter.getPrincipals();

        textUserValidThru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        EditUserActivity.this,
                        R.style.MyDatePickerStyle,
                        dateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        disableLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    userDisableloginValue = true;
                    Log.i("DISABLE LOGIN", "TRUE");

                } else {
                    userDisableloginValue = false;
                    Log.i("DISABLE LOGIN", "FALSE");
                }

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User oldUser = (User) getIntent().getSerializableExtra("user");
                int idToEdit = (int) getIntent().getSerializableExtra("toedit");
                User newUser = new User();

                /*Check if user change the principal*/
                String principalIdFromDropDown = principalDropdown.getText().toString().split("-")[0].trim();

                if (oldUser.getPrincipal().getPrincipalId().equalsIgnoreCase(principalIdFromDropDown)) {
                    newUser.setPrincipalId(oldUser.getPrincipal().getId());
                } else {
                    Principal getPrincipal = presenter.getPrincipalByString(principalDropdown.getText().toString());
                    newUser.setPrincipalId(getPrincipal.getId());
                }

                if (distributorDropdown.getText().toString().isEmpty()
                        || distributorDropdown.getText().toString().equalsIgnoreCase("-")
                        || distributorDropdown.getText().toString().equalsIgnoreCase("No Distributor"))
                {
                    newUser.setDistributorId(null);
                } else if (oldUser.getDistributor() != null &&
                           oldUser.getDistributor().getDistributorId()
                                  .equalsIgnoreCase(distributorDropdown.getText()
                                                                       .toString()
                                                                       .split("-")[0]
                                                                       .trim()))
                {
                    Log.i("OLD DIS", ""+ oldUser.getDistributor().getId());
                    newUser.setDistributorId(oldUser.getDistributor().getId());
                } else {
                    Distributor getDistributor = presenter.getDistributorByString(distributorDropdown.getText().toString());
                    newUser.setDistributorId(getDistributor.getId());
                }
                newUser.setId(idToEdit);
                newUser.setUserId(textUserId.getText().toString());
                newUser.setName(textName.getText().toString());
                newUser.setAddress(textAddress.getText().toString());
                newUser.setPhone(textPhone.getText().toString());
                newUser.setEmail(textEmail.getText().toString());
                newUser.setStatus(statusDropDown.getText().toString());
                newUser.setDisableLogin(userDisableloginValue);
                if (!textUserValidThru.getText().toString().equalsIgnoreCase("-")) {
                    try {
                        newUser.setUserValidThru(dateFormatter.formatStringToDate(textUserValidThru.getText().toString()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                presenter.editUser(textEmail, emailLyt, textUserId, textName, textAddress, textPhone,
                        userIdLyt, nameLyt, addressLyt, phoneLyt, principalLyt, newUser, sharedPreferences);
            }
        });

        principalDropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Principal principal = presenter.getPrincipalByString(parent.getItemAtPosition(position).toString());
                distributorDropdown.setText("");
                presenter.getDistributors(principal.getId());

            }
        });

        loadStatus();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();

    }

    @Override
    public void getData(User user) {
        String principalView = user.getPrincipal().getPrincipalId().toUpperCase() + " - " + user.getPrincipal().getName();
        String distributorView = presenter.getDistributor(user);
        principalDropdown.setText(principalView, false);
        //check and load distributor oncreate
        presenter.isDistributorActive(distriubtorLyt, user);
        presenter.getDistributors(user.getPrincipal().getId());
        distributorDropdown.setText(distributorView, false);
        textUserId.setText(user.getUserId().toUpperCase());
        textName.setText(user.getName());
        textAddress.setText(user.getAddress());
        textPhone.setText(user.getPhone());
        textEmail.setText(user.getEmail());
        statusDropDown.setText(user.getStatus(), false);
        textRegistrationDate.setText(dateFormatter.formatDateToString(user.getRegistrationDate()));
        textUserValidThru.setText(dateFormatter.formatDateToString(user.getUserValidThru()));
        textCreatedAt.setText(dateFormatter.formatDateToString(user.getCreatedAt()));
        textCreatedBy.setText(user.getCreatedBy());
        textUpdatedAt.setText(dateFormatter.formatDateToString(user.getUpdatedAt()));
        textUpdatedBy.setText(user.getUpdatedBy());
    }

    @Override
    public void onBackPressed() {
        AlertDialog deleteDialog = new AlertDialog.Builder(this)
                .setTitle("CONFIRMATION")
                .setMessage("Are you sure you want cancel edit?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditUserActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create();
        deleteDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onSuccess(String message) {
        finish();
    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadPrincipalItems(List<Principal> principalList) {
        for (Principal p : principalList) {
            String toShow = p.getPrincipalId().toUpperCase() + " - " + p.getName().toUpperCase();
            principalString.add(toShow);
        }
        ArrayAdapter<String> principalAdapter = new ArrayAdapter(this, R.layout.item_dropdown_principal, principalString);
        principalDropdown.setAdapter(principalAdapter);
    }

    public void loadStatus() {
        ArrayAdapter<String> statusAdapter = new ArrayAdapter(this, R.layout.item_dropdown_status, status);
        statusDropDown.setText(getString(R.string.status_regular), false);
        statusDropDown.setAdapter(statusAdapter);
    }

    @Override
    public void loadDistributorItems(List<String> distributorList) {
        ArrayAdapter<String> distributorAdapter = new ArrayAdapter(this, R.layout.item_dropdown_distributor, distributorList);
        distributorDropdown.setAdapter(distributorAdapter);
    }

    @Override
    public void switchDisableLoginOn() {
        disableLogin.setChecked(true);

    }

    @Override
    public void switchDisableLoginOff() {
        disableLogin.setChecked(false);

    }

    void init() {
        textUserId = binding.tvIdUser;
        textName = binding.tvUserName;
        textAddress = binding.tvUserAddress;
        textPhone = binding.tvUserPhone;
        textEmail = binding.tvUserEmail;
        statusDropDown = binding.userStatusDropdown;
        textRegistrationDate = binding.tvUserRegistrationdate;
        textUserValidThru = binding.tvUserValidthru;
        textCreatedAt = binding.tvUserCreatedat;
        textCreatedBy = binding.tvUserCreatedby;
        textUpdatedAt = binding.tvUserUpdatedat;
        textUpdatedBy = binding.tvUserUpdatedby;
        disableLogin = binding.smDisableLogin;

        userIdLyt = binding.userIdLayout;
        principalLyt = binding.principalLayout;
        nameLyt = binding.userNameLayout;
        addressLyt = binding.userAddressLayout;
        phoneLyt = binding.userPhoneLayout;
        distributorDropdown = binding.tvDistributor;
        principalDropdown = binding.tvPrincipal;
        emailLyt = binding.userEmailLayout;
        distriubtorLyt = binding.distributorIdLayout;
        btnSave = binding.btSaveUser;

        sharedPreferences = getSharedPreferences("nexchief", MODE_PRIVATE);
        presenter = new EditUserPresenter(this);

        dateFormatter = new DateFormatter();
        calendar = Calendar.getInstance();

        principalString = new ArrayList<>();

        textUserValidThru.setInputType(InputType.TYPE_NULL);


        //uneditable fields
        textUserId.setKeyListener(null);
        textCreatedAt.setKeyListener(null);
        textCreatedBy.setKeyListener(null);
        textUpdatedAt.setKeyListener(null);
        textUpdatedBy.setKeyListener(null);
        progressDialog = new ProgressDialog(this);

        status = new ArrayList<String>();
        status.add(getString(R.string.status_premium));
        status.add(getString(R.string.status_regular));
    }
}