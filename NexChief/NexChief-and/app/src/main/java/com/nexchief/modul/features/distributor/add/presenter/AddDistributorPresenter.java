package com.nexchief.modul.features.distributor.add.presenter;

import android.content.SharedPreferences;
import android.util.Patterns;
import android.widget.AutoCompleteTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.distributor.add.view.AddDistributorViewContract;
import com.nexchief.util.retrofit.services.DistributorService;
import com.nexchief.util.retrofit.services.PrincipalService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDistributorPresenter implements AddDistributorPresenterContract {

    private final AddDistributorViewContract view;
    private final DistributorService distributorService;
    private final PrincipalService principalService;
    private List<Principal> principalList;


    public AddDistributorPresenter(AddDistributorViewContract view) {
        this.view = view;
        this.distributorService = ServiceGenerator.createService(DistributorService.class);
        this.principalService = ServiceGenerator.createService(PrincipalService.class);
    }


    @Override
    public boolean anyChanges(AutoCompleteTextView principalDropDown, TextInputEditText name, TextInputEditText address,
                              TextInputEditText city, TextInputEditText firstName,
                              TextInputEditText lastName, TextInputEditText phone) {
        return !principalDropDown.getText().toString().isEmpty() ||
                !name.getText().toString().isEmpty() ||
                !address.getText().toString().isEmpty() ||
                !city.getText().toString().isEmpty() ||
                !firstName.getText().toString().isEmpty() ||
                !phone.getText().toString().isEmpty() ||
                !lastName.getText().toString().isEmpty();
    }

    @Override
    public void getPrincipals() {
        principalList = new ArrayList<>();
        principalService.getAllPrincipal().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONArray jArray = new JSONObject(response.body().string()).getJSONArray("data");

                    for (int i = 0; i < jArray.length(); i++) {
                        Principal principal = new Principal();
                        JSONObject temp = jArray.getJSONObject(i);
                        principal.setId(temp.getInt("id"));
                        principal.setName(temp.getString("name"));
                        principal.setPrincipalId(temp.getString("principalId"));
                        principalList.add(principal);
                    }
                    view.showPrincipals(principalList);
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public Principal getPrincipalByString(String string) {
        if (string.equalsIgnoreCase("---")) {
            return null;
        }
        String[] splitString = string.trim().split("-");
        if (!principalList.isEmpty()) {
            for (Principal p : principalList) {
                if (p.getPrincipalId().equalsIgnoreCase(splitString[0].trim())) {
                    return p;
                }
            }
        }
        return null;

    }

    @Override
    public void saveData(TextInputEditText distributorId, TextInputLayout distributorIdLyt, AutoCompleteTextView principalDropDown, TextInputEditText name,
                         TextInputEditText address, TextInputEditText city,
                         TextInputEditText firstName, TextInputEditText lastName,
                         TextInputEditText phone, TextInputLayout dropDownLayout,
                         TextInputLayout nameLayout,
                         TextInputLayout addressLayout, TextInputLayout cityLayout,
                         TextInputLayout firstNameLayout, TextInputLayout lastNameLayout,
                         TextInputLayout phoneLayout,
                         Distributor distributor, SharedPreferences sharedPreferences,
                         TextInputEditText email, TextInputLayout emailLayout) {

        if (validate(distributorId, distributorIdLyt, principalDropDown, name, address, city, firstName, lastName, phone, dropDownLayout,
                nameLayout, addressLayout, cityLayout, firstNameLayout, lastNameLayout, phoneLayout, email,
                emailLayout)) {

            distributor.setCreatedBy(sharedPreferences.getString("username", ""));
            distributor.setUpdatedBy(sharedPreferences.getString("username", ""));
            distributorService.addDistributor(distributor).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        view.onSuccess("Add New Distributor Sucessfull!");
                    } else {
                        try {
                            JSONObject errorObject = new JSONObject(response.errorBody().string());
                            view.onFailed(errorObject.getString("message"));
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    view.onFailed("Add New Distributor Failed");

                }
            });
        }


    }

    public boolean validate(TextInputEditText distributorId, TextInputLayout distributorIdLyt, AutoCompleteTextView principalDropDown, TextInputEditText name, TextInputEditText address, TextInputEditText city,
                            TextInputEditText firstName, TextInputEditText lastName,
                            TextInputEditText phone, TextInputLayout dropDownLayout, TextInputLayout nameLayout,
                            TextInputLayout addressLayout, TextInputLayout cityLayout,
                            TextInputLayout firstNameLayout, TextInputLayout lastNameLayout,
                            TextInputLayout phoneLayout, TextInputEditText email,
                            TextInputLayout emailLayout) {

        if (principalDropDown.getText().toString().isEmpty()) {
            dropDownLayout.setErrorEnabled(true);
            dropDownLayout.setError("* Principal is Required");
            return false;
        } else {
            dropDownLayout.setErrorEnabled(false);
        }

        if (!distributorId.getText().toString().isEmpty()
                && !Pattern.compile("^([A-Z]{5,12}|[A-Z0-9]{5,12})$").matcher(distributorId.getText().toString()).matches()) {
            distributorIdLyt.setErrorEnabled(true);
            distributorIdLyt.setError("ID Format Invalid, ID must contain 5 to 12 letters or number, or Empty");
        } else {
            distributorIdLyt.setErrorEnabled(false);
        }

        if (name.getText().toString().isEmpty()) {
            nameLayout.setErrorEnabled(true);
            nameLayout.setError("* Name is Required");
            return false;
        } else {
            nameLayout.setErrorEnabled(false);
        }

        if (address.getText().toString().isEmpty()) {
            addressLayout.setErrorEnabled(true);
            addressLayout.setError("* Address is Required");
            return false;
        } else {
            addressLayout.setErrorEnabled(false);
        }

        if (address.getText().toString().length() > 100) {
            addressLayout.setErrorEnabled(true);
            addressLayout.setError("* Address must less than 100 characters");
            return false;
        } else {
            addressLayout.setErrorEnabled(false);
        }

        if (city.getText().toString().isEmpty()) {
            cityLayout.setErrorEnabled(true);
            cityLayout.setError("* City is Required");
            return false;
        } else {
            cityLayout.setErrorEnabled(false);
        }

        if (!email.getText().toString().isEmpty()
                && !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            emailLayout.setErrorEnabled(true);
            emailLayout.setError("Email format is invalid");
            return false;
        } else {
            emailLayout.setErrorEnabled(false);
        }

        if (firstName.getText().toString().isEmpty()) {
            firstNameLayout.setErrorEnabled(true);
            firstNameLayout.setError("* Owner First Name is Required");
            return false;
        } else {
            firstNameLayout.setErrorEnabled(false);
        }

        if (lastName.getText().toString().isEmpty()) {
            lastNameLayout.setErrorEnabled(true);
            lastNameLayout.setError("* Owner Last Name is Required");
            return false;
        } else {
            lastNameLayout.setErrorEnabled(false);
        }

        if (phone.getText().toString().isEmpty()) {
            phoneLayout.setErrorEnabled(true);
            phoneLayout.setError("* Phone is Required");
            return false;
        } else {
            phoneLayout.setErrorEnabled(false);
        }
        return true;
    }
}

