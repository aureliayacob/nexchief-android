package com.nexchief.modul.features.user.detail;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.databinding.ActivityDetailUserBinding;
import com.nexchief.model.Distributor;
import com.nexchief.model.User;
import com.nexchief.modul.features.user.detail.presenter.DetailUserPresenter;
import com.nexchief.modul.features.user.detail.presenter.DetailUserPresenterContract;
import com.nexchief.modul.features.user.detail.vew.DetailUserViewContract;
import com.nexchief.util.DateFormatter;

import java.lang.reflect.Array;
import java.sql.Timestamp;

public class DetailUserActivity extends AppCompatActivity implements DetailUserViewContract {

    private DetailUserPresenter presenter;
    private ActivityDetailUserBinding binding;
    private TextInputEditText textPrincipal, textDistributor, textUserId, textName, textAddress,
            textPhone, textEmail, textStatus, textRegistrationDate, textUserValidThru,
            textCreatedAt, textCreatedBy, textUpdatedAt, textUpdatedBy;
    private TextInputLayout distributorLyt;
    private DateFormatter dateFormatter;
    private SwitchCompat disableLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailUserBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.detail_user);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        setContentView(root);

        init();
        presenter.getData(getIntent());

        boolean toDelete = (boolean) getIntent().getSerializableExtra("todelete");
        if (toDelete) {
            AlertDialog deleteDialog = new AlertDialog.Builder(this)
                    .setTitle("DELETE")
                    .setMessage("Are you sure to delete this User?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            User todelete = (User) getIntent().getSerializableExtra("user");
                            presenter.deleteUser(todelete.getId());
                        }
                    })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            finish();
                        }
                    }).create();
            deleteDialog.show();
            ;
        }

    }

    @Override
    public void getData(User user) {
        String principalView = user.getPrincipal().getPrincipalId().toUpperCase() + " - " + user.getPrincipal().getName();
        presenter.isDistributorActive(distributorLyt, user);
        String distributorView = presenter.getDistributor(user);
        textPrincipal.setText(principalView);
        textDistributor.setText(distributorView);
        textUserId.setText(user.getUserId().toUpperCase());
        textName.setText(user.getName());
        textAddress.setText(user.getAddress());
        textPhone.setText(user.getPhone());
        textEmail.setText(user.getEmail());
        textStatus.setText(user.getStatus());
        textRegistrationDate.setText(dateFormatter.formatDateToString(user.getRegistrationDate()));
        textUserValidThru.setText(dateFormatter.formatDateToString(user.getUserValidThru()));
        textCreatedAt.setText(dateFormatter.formatDateToString(user.getCreatedAt()));
        textCreatedBy.setText(user.getCreatedBy());
        textUpdatedAt.setText(dateFormatter.formatDateToString(user.getUpdatedAt()));
        textUpdatedBy.setText(user.getUpdatedBy());
    }

    @Override
    public void onSucces(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void switchDisableLoginOn() {
        disableLogin.setChecked(true);
    }

    @Override
    public void switchDisableLoginOff() {
        disableLogin.setChecked(false);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    void init() {
        presenter = new DetailUserPresenter(this);

        textPrincipal = binding.tvPrincipal;
        textDistributor = binding.tvDistributor;
        textUserId = binding.tvIdUser;
        textName = binding.tvUserName;
        textAddress = binding.tvUserAddress;
        textPhone = binding.tvUserPhone;
        textEmail = binding.tvUserEmail;
        textStatus = binding.tvUserStatus;
        textRegistrationDate = binding.tvUserRegistrationdate;
        textUserValidThru = binding.tvUserValidthru;
        textCreatedAt = binding.tvUserCreatedat;
        textCreatedBy = binding.tvUserCreatedby;
        textUpdatedAt = binding.tvUserUpdatedat;
        textUpdatedBy = binding.tvUserUpdatedby;
        disableLogin = binding.smDisableLogin;
        distributorLyt = binding.distributorLayout;

        dateFormatter = new DateFormatter();
    }
}