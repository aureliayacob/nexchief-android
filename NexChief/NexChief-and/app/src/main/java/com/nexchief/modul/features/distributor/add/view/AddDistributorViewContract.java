package com.nexchief.modul.features.distributor.add.view;

import com.nexchief.model.Principal;

import java.util.List;

public interface AddDistributorViewContract {

    void showPrincipals(List<Principal> principals);

    void onSuccess(String message);

    void onFailed(String message);
}
