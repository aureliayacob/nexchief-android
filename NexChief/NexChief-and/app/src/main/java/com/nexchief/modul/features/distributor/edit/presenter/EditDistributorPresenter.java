package com.nexchief.modul.features.distributor.edit.presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.distributor.edit.view.EditDistributorViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.DistributorService;
import com.nexchief.util.retrofit.services.PrincipalService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDistributorPresenter implements EditDistributorPresenterContract {

    private EditDistributorViewContract view;
    private final DistributorService distributorService;
    private final PrincipalService principalService;
    private List<Principal> principalList;
    private final DateFormatter formatter;

    public EditDistributorPresenter(EditDistributorViewContract view) {
        this.view = view;
        this.distributorService = ServiceGenerator.createService(DistributorService.class);
        this.principalService = ServiceGenerator.createService(PrincipalService.class);
        this.formatter = new DateFormatter();
    }

    @Override
    public void getData(Intent intent) {
        Distributor distributor = (Distributor) intent.getSerializableExtra("distributor");
        distributorService.getDistributorById(distributor.getDistributorId()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject objectOfData;
                    try {
                        objectOfData = new JSONObject(response.body().string()).getJSONObject("data");

                        JSONObject distributorObj = objectOfData.getJSONObject("distributor");
                        JSONObject principalObj = objectOfData.getJSONObject("principal");

                        Distributor distributor = new Distributor();
                        distributor.setPrincipalStringId(principalObj.getString("principalId"));
                        distributor.setPrincipalName(principalObj.getString("name"));
                        distributor.setDistributorId(distributorObj.getString("distributorId"));
                        distributor.setName(distributorObj.getString("name"));
                        distributor.setAddress(distributorObj.getString("address"));
                        distributor.setCity(distributorObj.getString("city"));
                        distributor.setCountry(distributorObj.getString("country"));
                        distributor.setOwnerFirstName(distributorObj.getString("ownerFirstName"));
                        distributor.setOwnerLastName(distributorObj.getString("ownerLastName"));
                        distributor.setEmail(distributorObj.getString("email"));
                        distributor.setPhone(distributorObj.getString("phone"));
                        distributor.setFax(distributorObj.getString("fax"));
                        distributor.setWebsite(distributorObj.getString("website"));
                        distributor.setCreatedAt(formatter.formatStringToDate(distributorObj.getString("createdAt")));
                        distributor.setCreatedBy(distributorObj.getString("createdBy"));
                        distributor.setUpdatedAt(formatter.formatStringToDate(distributorObj.getString("updatedAt")));
                        distributor.setUpdatedBy(distributorObj.getString("updatedBy"));
                        view.getData(distributor);
                    } catch (JSONException | IOException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.onFailed("Get Data Failed");
            }
        });
    }

    @Override
    public void getPrincipals() {
        principalList = new ArrayList<>();
        principalService.getAllPrincipal().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject data;
                    try {

                        data = new JSONObject(response.body().string());
                        JSONArray jArray = data.getJSONArray("data");
                        for (int i = 0; i < jArray.length(); i++) {
                            Principal principal = new Principal();
                            JSONObject temp = jArray.getJSONObject(i);

                            principal.setId(temp.getInt("id"));
                            principal.setName(temp.getString("name"));
                            principal.setPrincipalId(temp.getString("principalId"));
                            principalList.add(principal);
                        }
                        view.showPrincipals(principalList);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public Principal getPrincipalByString(String string) {
        String[] splitString = string.trim().split("-");
        if (!principalList.isEmpty()) {
            for (Principal p : principalList) {
                if (p.getPrincipalId().equalsIgnoreCase(splitString[0].trim())) {
                    return p;
                }
            }
        }
        return null;
    }


    @Override
    public void editDistributor(TextInputEditText name, TextInputEditText address, TextInputEditText city,
                                TextInputEditText firstName, TextInputEditText lastName, TextInputEditText phone,
                                TextInputLayout nameLayout, TextInputLayout addressLayout,
                                TextInputLayout cityLayout, TextInputLayout firstNameLayout,
                                TextInputLayout lastNameLayout, TextInputLayout phoneLayout,
                                Distributor distributor, SharedPreferences sharedPreferences) {
        if (validate(name, address, city, firstName, lastName, phone, nameLayout, addressLayout,
                cityLayout, firstNameLayout, lastNameLayout, phoneLayout)) {
            distributor.setUpdatedBy(sharedPreferences.getString("username", ""));
            distributorService.editDistributor(distributor.getId(), distributor).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        view.onSuccess("Edit Distributor Sucessfull!");
                    } else {
                        try {
                            JSONObject errorObject = new JSONObject(response.errorBody().string());
                            view.onFailed(errorObject.getString("message"));
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    view.onFailed("Edit Distributor Failed");
                }
            });
        }

    }

    @Override
    public void checkPrincipal(String id, TextInputLayout principalLayout, AutoCompleteTextView principal) {
        principalService.getPrincipalById(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.i("ADA tp expired","");
                    try {
                        Date licensedExpiryDate = formatter
                                .formatStringToDate(new JSONObject(response.body().string())
                                        .getJSONObject("data")
                                .getString("licensedExpiryDate"));
                        if (licensedExpiryDate.before(new Date())) {
                            principalLayout.setHelperTextEnabled(true);
                            principalLayout.setHelperText("* Principal is Inactive");
                            principal.setTextColor(Color.parseColor("#ff3333"));
                        } else {
                            principalLayout.setHelperTextEnabled(false);
                            principal.setTextColor(Color.parseColor("#000000"));
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    public boolean validate(TextInputEditText name, TextInputEditText address, TextInputEditText city,
                            TextInputEditText firstName, TextInputEditText lastName, TextInputEditText phone,
                            TextInputLayout nameLayout, TextInputLayout addressLayout, TextInputLayout cityLayout,
                            TextInputLayout firstNameLayout, TextInputLayout lastNameLayout, TextInputLayout phoneLayout) {

        if (name.getText().toString().isEmpty()) {
            nameLayout.setErrorEnabled(true);
            nameLayout.setError("* Name is Required");
            return false;
        }

        if (address.getText().toString().isEmpty()) {
            addressLayout.setErrorEnabled(true);
            addressLayout.setError("* Address is Required");
            return false;
        }

        if (address.getText().toString().length() > 100) {
            addressLayout.setErrorEnabled(true);
            addressLayout.setError("* Address must less than 100 characters");
            return false;
        }

        if (city.getText().toString().isEmpty()) {
            cityLayout.setErrorEnabled(true);
            cityLayout.setError("* City is Required");
            return false;
        }

        if (firstName.getText().toString().isEmpty()) {
            firstNameLayout.setErrorEnabled(true);
            firstNameLayout.setError("* Owner First Name is Required");
            return false;
        }

        if (lastName.getText().toString().isEmpty()) {
            lastNameLayout.setErrorEnabled(true);
            lastNameLayout.setError("* Owner Last Name is Required");
            return false;
        }

        if (phone.getText().toString().isEmpty()) {
            phoneLayout.setErrorEnabled(true);
            phoneLayout.setError("* Phone is Required");
            return false;
        }

        return true;
    }
}
