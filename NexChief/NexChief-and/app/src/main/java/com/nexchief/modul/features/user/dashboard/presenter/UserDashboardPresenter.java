package com.nexchief.modul.features.user.dashboard.presenter;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.model.User;
import com.nexchief.modul.features.user.dashboard.view.UserDashboardViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.UserService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDashboardPresenter implements UserDashboardPresenterContract {

    private final UserDashboardViewContract view;
    private final UserService userService;
    final ObjectMapper mapper;
    DateFormatter dateFormatter;

    public UserDashboardPresenter(UserDashboardViewContract view) {
        this.view = view;
        this.userService = ServiceGenerator.createService(UserService.class);
        mapper = new ObjectMapper();
        dateFormatter = new DateFormatter();
    }

    @Override
    public void getUser(int size, int page) {
        view.showProgressBar();
        userService.getAllUserPage(size, page).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.hideEmptyDataHelper();
                    view.hideProgressBar();
                    List<User> userList = new ArrayList<>();
                    JSONObject responseObject;
                    JSONArray arrayOfUser;
                    try {
                        responseObject = new JSONObject(response.body().string());
                        arrayOfUser = responseObject.getJSONArray("data");
                        for (int i = 0; i < arrayOfUser.length(); i++) {
                            JSONObject userObj = arrayOfUser.getJSONObject(i).getJSONObject("user");
                            JSONObject principalObj = arrayOfUser.getJSONObject(i).getJSONObject("principal");
                            JSONObject distributorObj = null;

                            if (!arrayOfUser.getJSONObject(i).isNull("distributor")) {
                                distributorObj = arrayOfUser.getJSONObject(i).getJSONObject("distributor");
                            }

                            Principal principal = new Principal(principalObj.getInt("id"),
                                    principalObj.getString("name"),
                                    principalObj.getString("principalId"));

                            Distributor distributor = null;
                            if (distributorObj != null) {
                                distributor = new Distributor(
                                        distributorObj.getInt("id"),
                                        distributorObj.getString("name"),
                                        distributorObj.getString("distributorId"));
                               if(!distributorObj.isNull("deletedAt")){
                                    distributor.setDeletedAt(dateFormatter.formatStringToDate(distributorObj.getString("deletedAt")));
                                }
                            }

                            User user = new User();
                            user.setPrincipal(principal);
                            user.setDistributor(distributor);
                            user.setId(userObj.getInt("id"));
                            user.setUserId(userObj.getString("userId"));
                            user.setName(userObj.getString("name"));
                                                        userList.add(user);
                        }
                        if (page > 0) {
                            view.updateUserList(userList);
                        } else {
                            view.showData(userList);
                        }

                    } catch (JSONException | IOException |ParseException  e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        view.onFailed(new JSONObject(response.errorBody().string()).getString("message"));
                        view.showEmptyDataHelper();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    view.hideProgressBar();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof SocketTimeoutException){
                    view.onFailed("Failed to connect to server");
                    view.showEmptyDataHelper();
                }
                view.hideProgressBar();
            }
        });


        userService.countUsers().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    int countedUser = 0;
                    try {
                        countedUser = new JSONObject(response.body().string()).getInt("data");
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    view.getTotalPage((int) Math.ceil(countedUser / size));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void searchUser(String keyword) {
        userService.searchUser(keyword).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.hideProgressBar();
                    List<User> userList = new ArrayList<>();
                    JSONObject responseObject;
                    JSONArray arrayOfUser;
                    try {
                        responseObject = new JSONObject(response.body().string());
                        arrayOfUser = responseObject.getJSONArray("data");
                        for (int i = 0; i < arrayOfUser.length(); i++) {
                            JSONObject dataUserObject = arrayOfUser.getJSONObject(i);
                            JSONObject userObj = dataUserObject.getJSONObject("user");
                            JSONObject principalObj = dataUserObject.getJSONObject("principal");
                            JSONObject distributorObj = null;

                            if (!dataUserObject.isNull("distributor")) {
                                distributorObj = dataUserObject.getJSONObject("distributor");
                            }

                            Principal principal = new Principal(principalObj.getInt("id"),
                                    principalObj.getString("name"),
                                    principalObj.getString("principalId"));

                            Distributor distributor = null;
                            if (distributorObj != null) {
                                distributor = new Distributor(
                                        distributorObj.getInt("id"),
                                        distributorObj.getString("name"),
                                        distributorObj.getString("distributorId"));
                                if(!distributorObj.isNull("deletedAt")){
                                    distributor.setDeletedAt(dateFormatter.formatStringToDate(distributorObj.getString("deletedAt")));
                                }
                            }

                            User user = new User();
                            user.setPrincipal(principal);
                            user.setDistributor(distributor);
                            user.setId(userObj.getInt("id"));
                            user.setUserId(userObj.getString("userId"));
                            user.setDisableLogin(userObj.getBoolean("disableLogin"));
                            user.setRegistrationDate(dateFormatter.formatStringToDate(userObj.getString("registrationDate")));
                            user.setStatus(userObj.getString("status"));
                            if (!userObj.isNull("userValidThru")) {
                                user.setUserValidThru(dateFormatter.formatStringToDate(userObj.getString("userValidThru")));
                            }
                            user.setName(userObj.getString("name"));
                            user.setAddress(userObj.getString("address"));
                            user.setEmail(userObj.getString("email"));
                            user.setPhone(userObj.getString("phone"));
                            user.setCreatedAt(dateFormatter.formatStringToDate(userObj.getString("createdAt")));
                            user.setCreatedBy(userObj.getString("createdBy"));
                            user.setUpdatedAt(dateFormatter.formatStringToDate(userObj.getString("updatedAt")));
                            user.setUpdatedBy(userObj.getString("updatedBy"));
                            userList.add(user);
                        }

                        view.getSearchResult(userList);


                    } catch (JSONException | IOException | ParseException e) {
                        e.printStackTrace();
                    }

                    view.onSuccess("");
                } else {
                    view.hideProgressBar();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.hideProgressBar();
            }
        });

    }
}
