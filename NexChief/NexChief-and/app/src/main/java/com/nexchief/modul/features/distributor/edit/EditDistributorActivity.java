package com.nexchief.modul.features.distributor.edit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.databinding.ActivityEditDistributorBinding;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.distributor.edit.presenter.EditDistributorPresenter;
import com.nexchief.modul.features.distributor.edit.view.EditDistributorViewContract;
import com.nexchief.util.DateFormatter;

import java.util.ArrayList;
import java.util.List;

public class EditDistributorActivity extends AppCompatActivity implements EditDistributorViewContract {

    private EditDistributorPresenter presenter;
    private ActivityEditDistributorBinding binding;
    private SharedPreferences sharedPreferences;

    private DateFormatter formatter;

    private AutoCompleteTextView principalDropDown;
    private List<String> principalString = new ArrayList<>();

    private TextInputEditText textDistributorId, textName, textAddress, textCity, textCountry,
            textOwnerFirstName, textOwnerLastName, textEmail, textPhone, textFax, textWeb,
            textCreatedAt, textCreatedBy, textUpdatedAt, textUpdatedBy;

    private TextInputLayout textNameLyt, textAddressLyt, textCityLyt, textOwnerFirstNameLyt,
            textOwnerLastNameLyt, textPhoneLyt, principalLyt;

    private Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditDistributorBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.edit_distributor);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        setContentView(root);

        init();

        presenter.getData(getIntent());
        presenter.getPrincipals();


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Distributor oldDistributor = (Distributor) getIntent().getSerializableExtra("distributor");
                int idToEdit = (int) getIntent().getSerializableExtra("toedit");
                Distributor editDistributor = new Distributor();
                if (oldDistributor.getPrincipalStringId().equalsIgnoreCase(principalDropDown.getText().toString().split("-")[0].trim())) {
                    editDistributor.setPrincipalId(oldDistributor.getPrincipalId());
                } else {
                    Principal getPrincipal = presenter.getPrincipalByString(principalDropDown.getText().toString());
                    Log.i("NEW", String.valueOf(getPrincipal.getId()));
                    editDistributor.setPrincipalId(getPrincipal.getId());
                }
                editDistributor.setId(idToEdit);
                editDistributor.setDistributorId(textDistributorId.getText().toString());
                editDistributor.setName(textName.getText().toString());
                editDistributor.setAddress(textAddress.getText().toString());
                editDistributor.setCity(textCity.getText().toString());
                editDistributor.setCountry(textCountry.getText().toString());
                editDistributor.setOwnerFirstName(textOwnerFirstName.getText().toString());
                editDistributor.setOwnerLastName(textOwnerLastName.getText().toString());
                editDistributor.setEmail(textEmail.getText().toString());
                editDistributor.setPhone(textPhone.getText().toString());
                editDistributor.setFax(textFax.getText().toString());
                editDistributor.setWebsite(textWeb.getText().toString());
                presenter.editDistributor(textName, textAddress, textCity, textOwnerFirstName, textOwnerLastName, textPhone,
                        textNameLyt, textAddressLyt, textCityLyt, textOwnerFirstNameLyt, textOwnerLastNameLyt, textPhoneLyt,
                        editDistributor, sharedPreferences);
            }
        });

        principalDropDown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Principal principal = presenter.getPrincipalByString(parent.getItemAtPosition(position).toString());
                presenter.checkPrincipal(principal.getPrincipalId(), principalLyt, principalDropDown);
            }
        });

    }

    @Override
    public void showPrincipals(List<Principal> principals) {
        for (Principal p : principals) {
            String toShow = p.getPrincipalId() + " - " + p.getName();
            this.principalString.add(toShow);

        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(this, R.layout.item_dropdown_principal, principalString);
        principalDropDown.setAdapter(arrayAdapter);
        principalDropDown.invalidate();
    }

    @Override
    public void getData(Distributor distributor) {
        principalDropDown.setText(distributor.getPrincipalStringId().toUpperCase() + " - " + distributor.getPrincipalName(), false);
        presenter.checkPrincipal(distributor.getPrincipalStringId(), principalLyt, principalDropDown);
        textDistributorId.setText(distributor.getDistributorId().toUpperCase());
        textName.setText(distributor.getName());
        textAddress.setText(distributor.getAddress());
        textCity.setText(distributor.getCity());
        textCountry.setText(distributor.getCountry());
        textOwnerFirstName.setText(distributor.getOwnerFirstName());
        textOwnerLastName.setText(distributor.getOwnerLastName());
        textEmail.setText(distributor.getEmail());
        textPhone.setText(distributor.getPhone());
        textFax.setText(distributor.getFax());
        textWeb.setText(distributor.getWebsite());
        textCreatedAt.setText(formatter.formatDateToString(distributor.getCreatedAt()));
        textCreatedBy.setText(distributor.getCreatedBy());
        textUpdatedAt.setText(formatter.formatDateToString(distributor.getUpdatedAt()));
        textUpdatedBy.setText(distributor.getUpdatedBy());

    }

    @Override
    public void onSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();

    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {
        AlertDialog deleteDialog = new AlertDialog.Builder(this)
                .setTitle("CONFIRMATION")
                .setMessage("Are you sure you want cancel edit?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditDistributorActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create();
        deleteDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    void init() {
        principalDropDown = binding.tvPrincipal;
        textDistributorId = binding.tvIdDistributor;
        textName = binding.tvDistributorName;
        textAddress = binding.tvDistributorAddress;
        textCity = binding.tvDistributorCity;
        textCountry = binding.tvDistributorCountry;
        textOwnerFirstName = binding.tvDistributorFirstname;
        textOwnerLastName = binding.tvDistributorLastname;
        textEmail = binding.tvDistributorEmail;
        textPhone = binding.tvDistributorPhone;
        textFax = binding.tvDistributorFax;
        textWeb = binding.tvDistributorWebsite;
        textCreatedAt = binding.tvDistributorCreatedat;
        textCreatedBy = binding.tvDistributorCreatedby;
        textUpdatedAt = binding.tvDistributorUpdatedat;
        textUpdatedBy = binding.tvDistributorUpdatedby;

        btnSave = binding.btSaveDistributor;

        textNameLyt = binding.distributorNameLayout;
        textAddressLyt = binding.distributorAddressLayout;
        textCityLyt = binding.distributorCityLayout;
        textOwnerFirstNameLyt = binding.distributorFirstnameLayout;
        textOwnerLastNameLyt = binding.distributorLastnameLayout;
        textPhoneLyt = binding.distributorPhoneLayout;
        principalLyt = binding.principalLayout;

        formatter = new DateFormatter();

        //uneditable fields
        textDistributorId.setKeyListener(null);
        textCreatedAt.setKeyListener(null);
        textCreatedBy.setKeyListener(null);
        textUpdatedAt.setKeyListener(null);
        textUpdatedBy.setKeyListener(null);

        presenter = new EditDistributorPresenter(this);
        sharedPreferences = getSharedPreferences("nexchief", MODE_PRIVATE);


    }


}