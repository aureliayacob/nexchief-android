package com.nexchief.modul.features.user.edit.presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Patterns;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.model.User;
import com.nexchief.modul.features.user.edit.view.EditUserViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.DistributorService;
import com.nexchief.util.retrofit.services.PrincipalService;
import com.nexchief.util.retrofit.services.UserService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditUserPresenter implements EditUserPresenterContract {
    private EditUserViewContract view;
    private UserService userService;
    private PrincipalService principalService;
    private DistributorService distributorService;
    private List<Principal> principalList;
    private List<Distributor> distributorList;
    private User toEdit;
    private final DateFormatter formatter;

    public EditUserPresenter(EditUserViewContract view) {
        this.view = view;
        this.userService = ServiceGenerator.createService(UserService.class);
        this.principalService = ServiceGenerator.createService(PrincipalService.class);
        this.distributorService = ServiceGenerator.createService(DistributorService.class);
        this.principalList = new ArrayList<Principal>();
        this.distributorList = new ArrayList<>();
        this.formatter = new DateFormatter();
    }


    @Override
    public void getData(Intent intent) {
        toEdit = (User) intent.getSerializableExtra("user");
        userService.getUserById(toEdit.getId()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject dataUserObject;
                    try {
                        dataUserObject = new JSONObject(response.body().string()).getJSONObject("data");
                        JSONObject userObj = dataUserObject.getJSONObject("user");
                        JSONObject principalObj = dataUserObject.getJSONObject("principal");
                        JSONObject distributorObj = null;

                        if (!dataUserObject.isNull("distributor")) {
                            distributorObj = dataUserObject.getJSONObject("distributor");
                        }

                        Principal principal = new Principal(principalObj.getInt("id"),
                                principalObj.getString("name"),
                                principalObj.getString("principalId"));

                        Distributor distributor = null;
                        if (distributorObj != null) {
                            distributor = new Distributor(
                                    distributorObj.getInt("id"),
                                    distributorObj.getString("name"),
                                    distributorObj.getString("distributorId"));
                            if(!distributorObj.isNull("deletedAt")){
                                distributor.setDeletedAt(formatter.formatStringToDate(distributorObj.getString("deletedAt")));
                            }
                        }


                        User user = new User();
                        user.setPrincipal(principal);
                        user.setDistributor(distributor);
                        user.setId(userObj.getInt("id"));
                        user.setUserId(userObj.getString("userId"));
                        user.setDisableLogin(userObj.getBoolean("disableLogin"));
                        if (user.isDisableLogin()) {
                            view.switchDisableLoginOn();
                        } else {
                            view.switchDisableLoginOff();
                        }
                        user.setRegistrationDate(formatter.formatStringToDate(userObj.getString("registrationDate")));
                        user.setStatus(userObj.getString("status"));
                        if (!userObj.isNull("userValidThru")) {
                            user.setUserValidThru(formatter.formatStringToDate(userObj.getString("userValidThru")));
                        }
                        user.setName(userObj.getString("name"));
                        user.setAddress(userObj.getString("address"));
                        user.setEmail(userObj.getString("email"));
                        user.setPhone(userObj.getString("phone"));
                        user.setCreatedAt(formatter.formatStringToDate(userObj.getString("createdAt")));
                        user.setCreatedBy(userObj.getString("createdBy"));
                        user.setUpdatedAt(formatter.formatStringToDate(userObj.getString("updatedAt")));
                        user.setUpdatedBy(userObj.getString("updatedBy"));

                        view.getData(user);
                    } catch (JSONException | IOException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public String getDistributor(User user) {
        String distributorView = "No Distributor";
        if (user.getDistributor() == null) {
            return distributorView;
        } else {
            distributorView = user.getDistributor().getDistributorId().toUpperCase() + " - " +
                    user.getDistributor().getName();
            return distributorView;
        }
    }


    @Override
    public void editUser(TextInputEditText email, TextInputLayout emailLyt,
                         TextInputEditText userId, TextInputEditText name,
                         TextInputEditText address, TextInputEditText phone,
                         TextInputLayout userIdLyt, TextInputLayout nameLyt,
                         TextInputLayout addressLyt, TextInputLayout phoneLyt,
                         TextInputLayout principalLyt, User user,
                         SharedPreferences sharedPreferences) {

        if (validate(email, emailLyt, userId, name, address, phone, userIdLyt, nameLyt,
                addressLyt, phoneLyt, principalLyt)) {
            user.setUpdatedBy(sharedPreferences.getString("username", ""));
            userService.editUser(toEdit.getId(), user).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        view.onSuccess("Edit User Successful!");
                    } else {
                        try {
                            JSONObject errorObject = new JSONObject(response.errorBody().string());
                            view.onFailed(errorObject.getString("message"));
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    view.onFailed("Edit User Failed");
                }
            });
        }
    }

    @Override
    public void getPrincipals() {
        principalService.getAllPrincipal().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject data;
                try {
                    data = new JSONObject(response.body().string());
                    JSONArray jArray = data.getJSONArray("data");
                    for (int i = 0; i < jArray.length(); i++) {
                        Principal principal = new Principal();
                        JSONObject temp = jArray.getJSONObject(i);
                        principal.setId(temp.getInt("id"));
                        principal.setName(temp.getString("name"));
                        principal.setPrincipalId(temp.getString("principalId"));

                        principalList.add(principal);
                    }
                    view.loadPrincipalItems(principalList);


                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    @Override
    public void getDistributors(int id) {
        List<String> distributorString = new ArrayList<>();
        distributorList.clear();
        distributorString.add("No Distributor");
        distributorService.getDistributorPerPrincipal(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject data;
                    try {
                        data = new JSONObject(response.body().string());
                        JSONArray jArray = data.getJSONArray("data");
                        for (int i = 0; i < jArray.length(); i++) {
                            Distributor distributor = new Distributor();
                            JSONObject temp = jArray.getJSONObject(i).getJSONObject("distributor");
                            distributor.setId(temp.getInt("id"));
                            distributor.setDistributorId(temp.getString("distributorId"));
                            distributor.setName(temp.getString("name"));
                            distributor.setDistributorId(temp.getString("distributorId"));
                            distributorList.add(distributor);
                        }
                        for (Distributor d : distributorList) {
                            String toShow = d.getDistributorId().toUpperCase() + " - " + d.getName().toUpperCase();
                            distributorString.add(toShow);
                        }
                        view.loadDistributorItems(distributorString);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    distributorString.clear();
                    distributorString.add("No Distributor");
                    view.loadDistributorItems(distributorString);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }


    @Override
    public Principal getPrincipalByString(String string) {
        String[] splitString = string.trim().split("-");
        if (!principalList.isEmpty()) {
            for (Principal p : principalList) {
                if (p.getPrincipalId().equalsIgnoreCase(splitString[0].trim())) {
                    return p;
                }
            }
        }
        return null;

    }

    @Override
    public Distributor getDistributorByString(String string) {
        if (string.equalsIgnoreCase("-")) {
            return null;
        }

        String[] splitString = string.trim().split("-");
        if (!distributorList.isEmpty()) {
            for (Distributor d : distributorList) {
                if (d.getDistributorId().equalsIgnoreCase(splitString[0].trim())) {
                    return d;
                }
            }
        }
        return null;
    }

    public boolean validate(TextInputEditText email, TextInputLayout emailLyt, TextInputEditText userId, TextInputEditText name,
                            TextInputEditText address, TextInputEditText phone,
                            TextInputLayout userIdLyt, TextInputLayout nameLyt,
                            TextInputLayout addressLyt, TextInputLayout phoneLyt, TextInputLayout principalLyt
    ) {
        if (userId.getText().toString().isEmpty()) {
            userIdLyt.setErrorEnabled(true);
            userIdLyt.setError("* UserId is Required");
            return false;
        } else if (!Pattern.compile("^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$").matcher(userId.getText().toString()).matches()) {
            userIdLyt.setErrorEnabled(true);
            userIdLyt.setError("* Username format invalid :consists of alphanumeric character");
            return false;
        } else {
            userIdLyt.setErrorEnabled(false);
        }


        if (name.getText().toString().isEmpty()) {
            nameLyt.setErrorEnabled(true);
            nameLyt.setError("* Name is Required");
            return false;
        } else {
            nameLyt.setErrorEnabled(false);
        }

        if (address.getText().toString().isEmpty()) {
            addressLyt.setErrorEnabled(true);
            addressLyt.setError("* Address is Required");
            return false;
        } else if (address.getText().toString().length() >= 100) {
            addressLyt.setErrorEnabled(true);
            addressLyt.setError("* Address must less than 100 characters");
            return false;
        } else {
            addressLyt.setErrorEnabled(false);
        }

        if (phone.getText().toString().isEmpty()) {
            phoneLyt.setErrorEnabled(true);
            phoneLyt.setError("* Phone is Required");
            return false;
        } else if (!Patterns.PHONE.matcher(phone.getText().toString()).matches()) {
            phoneLyt.setErrorEnabled(true);
            phoneLyt.setError("* Invalid phone number format");
            return false;
        } else {
            phoneLyt.setErrorEnabled(false);
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches() && !email.getText().toString().isEmpty()) {
            emailLyt.setErrorEnabled(true);
            emailLyt.setError("* Invalid email format ");
            return false;
        } else {
            emailLyt.setErrorEnabled(false);
        }

        return true;
    }

    public void isDistributorActive(TextInputLayout distributorLyt, User user) {
//      TODO -- Refining - DONE
        if (user.getDistributor() != null) {
            distributorService.getDistributorById(user.getDistributor().getDistributorId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (!response.isSuccessful()) {
                        distributorLyt.setHelperTextEnabled(true);
                        distributorLyt.setHelperText("Distributor is Nonactive");
                    }else{
                        distributorLyt.setHelperTextEnabled(false);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }else{
            distributorLyt.setHelperTextEnabled(false);
        }
    }
}

