package com.nexchief.modul.features.user.detail.presenter;

import android.content.Intent;
import android.util.Log;

import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.model.User;
import com.nexchief.modul.features.user.detail.vew.DetailUserViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.DistributorService;
import com.nexchief.util.retrofit.services.UserService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailUserPresenter implements DetailUserPresenterContract {
    private DetailUserViewContract view;
    private UserService userService;
    private final DateFormatter formatter;
    private final DistributorService distributorService;


    public DetailUserPresenter(DetailUserViewContract view) {
        this.view = view;
        this.userService = ServiceGenerator.createService(UserService.class);
        this.distributorService = ServiceGenerator.createService(DistributorService.class);
        this.formatter = new DateFormatter();

    }


    @Override
    public String getDistributor(User user) {
        String distributorView = "No Distributor";
        if (user.getDistributor() == null) {
            return distributorView;
        } else {
            distributorView = user.getDistributor().getDistributorId().toUpperCase() + " - " +
                    user.getDistributor().getName();
        }
        return distributorView;
    }

    @Override
    public void getData(Intent intent) {
        User user = (User) intent.getSerializableExtra("user");
        userService.getUserById(user.getId()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject dataUserObject;
                    JSONArray arrayOfUser;
                    try {
                        dataUserObject = new JSONObject(response.body().string()).getJSONObject("data");
                        JSONObject userObj = dataUserObject.getJSONObject("user");
                        JSONObject principalObj = dataUserObject.getJSONObject("principal");
                        JSONObject distributorObj = null;

                        if (!dataUserObject.isNull("distributor")) {
                            distributorObj = dataUserObject.getJSONObject("distributor");
                        }

                        Principal principal = new Principal(principalObj.getInt("id"),
                                principalObj.getString("name"),
                                principalObj.getString("principalId"));

                        Distributor distributor = null;

                        if (distributorObj != null) {
                            distributor = new Distributor(
                                    distributorObj.getInt("id"),
                                    distributorObj.getString("name"),
                                    distributorObj.getString("distributorId"));

                            if (!distributorObj.isNull("deletedAt")) {
                                distributor.setDeletedAt(formatter.formatStringToDate(distributorObj.getString("deletedAt")));
                            }
                        }


                        User user = new User();
                        user.setPrincipal(principal);
                        user.setDistributor(distributor);
                        user.setId(userObj.getInt("id"));
                        user.setUserId(userObj.getString("userId"));
                        user.setDisableLogin(userObj.getBoolean("disableLogin"));
                        if (user.isDisableLogin()) {
                            view.switchDisableLoginOn();
                        } else {
                            view.switchDisableLoginOff();
                        }
                        user.setRegistrationDate(formatter.formatStringToDate(userObj.getString("registrationDate")));
                        user.setStatus(userObj.getString("status"));
                        if (!userObj.isNull("userValidThru")) {
                            user.setUserValidThru(formatter.formatStringToDate(userObj.getString("userValidThru")));
                        }
                        user.setName(userObj.getString("name"));
                        user.setAddress(userObj.getString("address"));
                        user.setEmail(userObj.getString("email"));
                        user.setPhone(userObj.getString("phone"));
                        user.setCreatedAt(formatter.formatStringToDate(userObj.getString("createdAt")));
                        user.setCreatedBy(userObj.getString("createdBy"));
                        user.setUpdatedAt(formatter.formatStringToDate(userObj.getString("updatedAt")));
                        user.setUpdatedBy(userObj.getString("updatedBy"));

                        view.getData(user);
                    } catch (JSONException | IOException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    @Override
    public void deleteUser(int id) {
        userService.deleteUser(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.onSucces("Delete Success");
                } else {
                    view.onFailed("Delete Failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public void isDistributorActive(TextInputLayout distributorLyt, User user) {
//      TODO -- Refining - DONE
        if (user.getDistributor() != null) {
            distributorService.getDistributorById(user.getDistributor().getDistributorId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (!response.isSuccessful()) {
                        distributorLyt.setHelperTextEnabled(true);
                        distributorLyt.setHelperText("Distributor is Nonactive");
                    }else{
                        distributorLyt.setHelperTextEnabled(false);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }else{
            distributorLyt.setHelperTextEnabled(false);
       }
    }
}
