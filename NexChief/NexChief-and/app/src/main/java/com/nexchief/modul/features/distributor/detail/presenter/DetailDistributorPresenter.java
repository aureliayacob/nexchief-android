package com.nexchief.modul.features.distributor.detail.presenter;

import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Distributor;
import com.nexchief.modul.features.distributor.detail.view.DetailDistributorViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.DistributorService;
import com.nexchief.util.retrofit.services.PrincipalService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailDistributorPresenter implements DetailDistributorPresenterContract {
    private final DetailDistributorViewContract view;
    private final DistributorService distributorService;
    private final PrincipalService principalService;
    private final DateFormatter dateFormatter;

    public DetailDistributorPresenter(DetailDistributorViewContract view) {
        this.view = view;
        this.distributorService = ServiceGenerator.createService(DistributorService.class);
        this.dateFormatter = new DateFormatter();
        this.principalService = ServiceGenerator.createService(PrincipalService.class);
    }


    @Override
    public void getData(Intent intent) {
        Distributor distributor = (Distributor) intent.getSerializableExtra("distributor");
        distributorService.getDistributorById(distributor.getDistributorId()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    JSONObject objectOfData = null;
                    try {
                        objectOfData = new JSONObject(response.body().string()).getJSONObject("data");

                        JSONObject distributorObj = objectOfData.getJSONObject("distributor");
                        JSONObject principalObj = objectOfData.getJSONObject("principal");

                        Distributor distributor = new Distributor();
                        distributor.setPrincipalStringId(principalObj.getString("principalId"));
                        distributor.setPrincipalName(principalObj.getString("name"));
                        distributor.setDistributorId(distributorObj.getString("distributorId"));
                        distributor.setName(distributorObj.getString("name"));
                        distributor.setAddress(distributorObj.getString("address"));
                        distributor.setCity(distributorObj.getString("city"));
                        distributor.setCountry(distributorObj.getString("country"));
                        distributor.setOwnerFirstName(distributorObj.getString("ownerFirstName"));
                        distributor.setOwnerLastName(distributorObj.getString("ownerLastName"));
                        distributor.setEmail(distributorObj.getString("email"));
                        distributor.setPhone(distributorObj.getString("phone"));
                        distributor.setFax(distributorObj.getString("fax"));
                        distributor.setWebsite(distributorObj.getString("website"));
                        distributor.setCreatedAt(dateFormatter.formatStringToDate(distributorObj.getString("createdAt")));
                        distributor.setCreatedBy(distributorObj.getString("createdBy"));
                        distributor.setUpdatedAt(dateFormatter.formatStringToDate(distributorObj.getString("updatedAt")));
                        distributor.setUpdatedBy(distributorObj.getString("updatedBy"));
                        view.getDetailDistributor(distributor);
                    } catch (JSONException | IOException | ParseException e) {
                        e.printStackTrace();
                    }

                } else {
                    view.onFailed("Get Data Failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.onFailed("Get Data Failed");
            }
        });


    }

    @Override
    public void deleteDistributor(int id) {
        distributorService.deleteDistributor(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.onSuccess("Delete Success");
                } else {
                    view.onFailed("Delete Failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.onFailed("Delete Failed");
            }
        });

    }

    @Override
    public void checkPrincipal(String id, TextInputLayout principalLayout, TextInputEditText principal) {
        principalService.getPrincipalById(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.i("ADA tp expired","");
                    try {
                        Date licensedExpiryDate = dateFormatter
                                .formatStringToDate(new JSONObject(response.body().string())
                                        .getJSONObject("data")
                                        .getString("licensedExpiryDate"));
                        if (licensedExpiryDate.before(new Date())) {
                            principalLayout.setHelperTextEnabled(true);
                            principalLayout.setHelperText("* Principal is Inactive");
                            principal.setTextColor(Color.parseColor("#ff3333"));
                        } else {
                            principalLayout.setHelperTextEnabled(false);
                            principal.setTextColor(Color.parseColor("#000000"));
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
