package com.nexchief.modul.features.distributor.dashboard.view;

import com.nexchief.model.Distributor;

import java.util.List;

public interface DistributorDashboardViewContract {

    void updateDistributorList(List<Distributor> distributorList);

    void updateFilteredList(List<Distributor> distributorList);

    void showAllDistributor(List<Distributor> distributorList);

    void showProgressBar();

    void hideProgressBar();

    void onSuccess();

    void onFailed(String message);

    void getTotalPage(int totalPage);

    void showEmptyDataHelper();

    void hideEmptyDataHelper();
}
