package com.nexchief.modul.features.user.edit.view;

import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.model.User;

import java.util.List;

public interface EditUserViewContract {
    void showProgressDialog();

    void hideProgressDialog();

    void getData(User user);

    void onSuccess(String message);

    void onFailed(String message);

    void loadPrincipalItems(List<Principal> principalList);

    void loadDistributorItems(List<String> distributorList);

    void switchDisableLoginOn();

    void switchDisableLoginOff();

}
