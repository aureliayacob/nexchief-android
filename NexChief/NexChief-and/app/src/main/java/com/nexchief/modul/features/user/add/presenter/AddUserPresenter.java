package com.nexchief.modul.features.user.add.presenter;

import android.content.SharedPreferences;
import android.util.Log;
import android.util.Patterns;
import android.widget.AutoCompleteTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.model.User;
import com.nexchief.modul.features.user.add.view.AddUserViewContract;
import com.nexchief.util.retrofit.services.DistributorService;
import com.nexchief.util.retrofit.services.PrincipalService;
import com.nexchief.util.retrofit.services.UserService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddUserPresenter implements AddUserPresenterContract {
    private AddUserViewContract view;
    private UserService userService;
    private PrincipalService principalService;
    private DistributorService distributorService;
    private List<Principal> principalList;
    private List<Distributor> distributorList;

    public AddUserPresenter(AddUserViewContract view) {
        this.view = view;
        this.userService = ServiceGenerator.createService(UserService.class);
        this.principalService = ServiceGenerator.createService(PrincipalService.class);
        this.distributorService = ServiceGenerator.createService(DistributorService.class);
    }

    @Override
    public void getPrincipals() {
        principalList = new ArrayList<>();
        principalService.getAllPrincipal().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject data = null;
                try {
                    data = new JSONObject(response.body().string());
                    JSONArray jArray = data.getJSONArray("data");

                    for (int i = 0; i < jArray.length(); i++) {
                        Principal principal = new Principal();
                        JSONObject temp = jArray.getJSONObject(i);
                        principal.setId(temp.getInt("id"));
                        principal.setName(temp.getString("name"));
                        principal.setPrincipalId(temp.getString("principalId"));
                        principalList.add(principal);
                    }
                    view.showPrincipals(principalList);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    @Override
    public void getDistributors(int id) {

        distributorList = new ArrayList<>();
        List<String> distributorString = new ArrayList<>();
        distributorString.add("No Distributor");
        distributorService.getDistributorPerPrincipal(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    JSONObject data = null;
                    try {
                        data = new JSONObject(response.body().string());
                        JSONArray jArray = data.getJSONArray("data");

                        for (int i = 0; i < jArray.length(); i++) {
                            Distributor distributor = new Distributor();
                            JSONObject temp = jArray.getJSONObject(i).getJSONObject("distributor");

                            distributor.setId(temp.getInt("id"));
                            distributor.setDistributorId(temp.getString("distributorId"));
                            distributor.setName(temp.getString("name"));
                            distributor.setDistributorId(temp.getString("distributorId"));
                            distributorList.add(distributor);
                        }
                        for (Distributor d : distributorList) {
                            String toShow = d.getDistributorId().toUpperCase() + " - " + d.getName().toUpperCase();
                            distributorString.add(toShow);
                        }
                        view.showDistributors(distributorString);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    distributorString.clear();
                    distributorString.add("No Distributor");
                    view.showDistributors(distributorString);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public Principal getPrincipalByString(String string) {
        String[] splitString = string.trim().split("-");
        if (!principalList.isEmpty()) {
            for (Principal p : principalList) {
                if (p.getPrincipalId().equalsIgnoreCase(splitString[0].trim())) {
                    return p;
                }
            }
        }
        return null;

    }

    @Override
    public Distributor getDistributorByString(String string) {
        if (string.equalsIgnoreCase("---")
                || string.isEmpty()
                || string.equalsIgnoreCase("No Distributor")) {
            return null;
        }

        String[] splitString = string.trim().split("-");
        if (!distributorList.isEmpty()) {
            for (Distributor d : distributorList) {
                if (d.getDistributorId().equalsIgnoreCase(splitString[0].trim())) {
                    return d;
                }
            }
        }
        return null;
    }

    public boolean validate(TextInputEditText email, TextInputLayout emailLyt, TextInputEditText validThru,
                            TextInputLayout validThruLyt, AutoCompleteTextView principaldd, TextInputEditText userId,
                            TextInputEditText name, TextInputEditText password,
                            TextInputEditText address, TextInputEditText phone, Principal principal,
                            TextInputLayout userIdLyt, TextInputLayout nameLyt, TextInputLayout passwordLyt,
                            TextInputLayout addressLyt, TextInputLayout phoneLyt, TextInputLayout principalLyt) {

        if (principaldd.getText().toString().isEmpty() || principal == null) {
            principalLyt.setErrorEnabled(true);
            principalLyt.setError("* Principal is Required");
            return false;
        } else {
            principalLyt.setErrorEnabled(false);
        }

        if (userId.getText().toString().isEmpty()) {
            userIdLyt.setErrorEnabled(true);
            userIdLyt.setError("* UserId is Required");
            return false;
        } else {
            userIdLyt.setErrorEnabled(false);
        }

        if (!Pattern.compile("^([A-Z]{5,12}|[A-Z0-9]{5,12})$").matcher(userId.getText().toString()).matches()) {
            userIdLyt.setErrorEnabled(true);
            userIdLyt.setError("* Username format invalid : Username consists of minimum 5 to 12 alphanumeric character");
            return false;
        } else {
            userIdLyt.setErrorEnabled(false);
        }

        if (name.getText().toString().isEmpty()) {
            nameLyt.setErrorEnabled(true);
            nameLyt.setError("* Name is Required");
            return false;
        } else {
            nameLyt.setErrorEnabled(false);
        }

        String passwordPattern = "^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)[A-Za-z\\d]{8,24}$";
        if (password.getText().toString().isEmpty()) {
            passwordLyt.setErrorEnabled(true);
            passwordLyt.setError("* Password is Required");
            return false;
        } else if (!Pattern.compile(passwordPattern).matcher(password.getText().toString()).matches()) {
            passwordLyt.setErrorEnabled(true);
            passwordLyt.setError("* Invalid Password, Password must be minimum 8 characters, at least 1  uppercase letter and 1 number");
            return false;
        } else {
            passwordLyt.setErrorEnabled(false);
        }

        if (address.getText().toString().isEmpty()) {
            addressLyt.setErrorEnabled(true);
            addressLyt.setError("* Address is Required");
            return false;
        } else if (address.getText().toString().length() >= 100) {
            addressLyt.setErrorEnabled(true);
            addressLyt.setError("* Address must less than 100 characters");
            return false;
        } else {
            addressLyt.setErrorEnabled(false);
        }

        if (phone.getText().toString().isEmpty()) {
            phoneLyt.setErrorEnabled(true);
            phoneLyt.setError("* Phone is Required");
            return false;
        } else if (!Patterns.PHONE.matcher(phone.getText().toString()).matches()) {
            phoneLyt.setErrorEnabled(true);
            phoneLyt.setError("* Invalid phone number format");
            return false;
        } else {
            phoneLyt.setErrorEnabled(false);
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches() && !email.getText().toString().isEmpty()) {
            emailLyt.setErrorEnabled(true);
            emailLyt.setError("* Invalid email format ");
            return false;
        } else {
            emailLyt.setErrorEnabled(false);
        }

        return true;
    }

    @Override
    public boolean anyChanges(AutoCompleteTextView principalDropDown, TextInputEditText userId,
                              TextInputEditText name, TextInputEditText password,
                              TextInputEditText address, TextInputEditText phone) {
        if (!principalDropDown.getText().toString().isEmpty() ||
                !userId.getText().toString().isEmpty() ||
                !name.getText().toString().isEmpty() ||
                !password.getText().toString().isEmpty() ||
                !address.getText().toString().isEmpty() ||
                !phone.getText().toString().isEmpty()) {
            return true;
        }
        return false;
    }


    public void saveData(TextInputEditText email, TextInputLayout emailLyt, TextInputEditText validThru,
                         TextInputLayout validThruLyt, AutoCompleteTextView principaldd, TextInputEditText userId,
                         TextInputEditText name, TextInputEditText password,
                         TextInputEditText address, TextInputEditText phone, Principal principal,
                         TextInputLayout userIdLyt, TextInputLayout nameLyt, TextInputLayout passwordLyt,
                         TextInputLayout addressLyt, TextInputLayout phoneLyt, TextInputLayout principalLyt,
                         User user, SharedPreferences sharedPreferences) {
        if (validate(email, emailLyt, validThru, validThruLyt, principaldd, userId, name, password,
                address, phone, principal, userIdLyt, nameLyt, passwordLyt, addressLyt, phoneLyt, principalLyt)) {

            user.setCreatedBy(sharedPreferences.getString("username", ""));
            user.setUpdatedBy(sharedPreferences.getString("username", ""));
            userService.addNewUser(user).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        view.onSuccess("Add New User Successfull!");
                    } else {
                        try {
                            JSONObject errorObject = new JSONObject(response.errorBody().string());
                            view.onFailed(errorObject.getString("message"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    view.onFailed("Add New User Failed");

                }
            });
        }


    }
}
