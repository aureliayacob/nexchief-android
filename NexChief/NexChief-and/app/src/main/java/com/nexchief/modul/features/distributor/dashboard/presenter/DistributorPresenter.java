package com.nexchief.modul.features.distributor.dashboard.presenter;

import android.util.Log;

import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Distributor;
import com.nexchief.modul.features.distributor.dashboard.view.DistributorDashboardViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.DistributorService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DistributorPresenter implements DistributorDashboardPresenterContract {
    private final DateFormatter dateFormatter;
    private final DistributorDashboardViewContract view;
    private final DistributorService distributorService;

    public DistributorPresenter(DistributorDashboardViewContract view) {
        this.view = view;
        this.distributorService = ServiceGenerator.createService(DistributorService.class);
        dateFormatter = new DateFormatter();
    }


    @Override
    public void getDistributor(int size, int page) {
        view.showProgressBar();
        distributorService.getAllDistributorPage(size, page).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.hideEmptyDataHelper();
                    view.hideProgressBar();
                    JSONObject responseObject;

                    try {
                        responseObject = new JSONObject(response.body().string());
                        JSONArray getArrayOfData = (responseObject.getJSONArray("data"));

                        List<Distributor> distributorList = new ArrayList<>();
                        for (int i = 0; i < getArrayOfData.length(); i++) {
                            Distributor distributor = new Distributor();
                            JSONObject distributorObj = getArrayOfData.getJSONObject(i).getJSONObject("distributor");
                            distributor.setId(distributorObj.getInt("id"));
                            distributor.setDistributorId(distributorObj.getString("distributorId"));
                            distributor.setName(distributorObj.getString("name"));
                            distributor.setPrincipalStringId(getArrayOfData.getJSONObject(i).getJSONObject("principal").getString("principalId"));
                            distributor.setPrincipalId(getArrayOfData.getJSONObject(i).getJSONObject("principal").getInt("id"));
                            distributorList.add(distributor);
                        }
                        if (page > 0) {
                            view.updateDistributorList(distributorList);

                        } else {
                            view.showAllDistributor(distributorList);
                        }

                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        view.onFailed(new JSONObject(response.errorBody().string()).getString("message"));
                        view.showEmptyDataHelper();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    view.hideProgressBar();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    view.onFailed("Failed to connect to server");
                }
                view.hideProgressBar();
            }
        });

        distributorService.countDistributor().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    int countedDistributor = 0;
                    try {
                        countedDistributor = new JSONObject(response.body().string()).getInt("data");
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    view.getTotalPage((int) Math.ceil(countedDistributor / size));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public void searchDistributor(String keyword) {
        distributorService.searchDistributor(keyword).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.hideProgressBar();
                    JSONObject responseObject;
                    try {
                        responseObject = new JSONObject(response.body().string());
                        JSONArray getArrayOfData = (responseObject.getJSONArray("data"));

                        List<Distributor> distributorList = new ArrayList<>();
                        for (int i = 0; i < getArrayOfData.length(); i++) {
                            Distributor distributor = new Distributor();
                            JSONObject distributorObj = getArrayOfData.getJSONObject(i).getJSONObject("distributor");
                            distributor.setId(distributorObj.getInt("id"));
                            distributor.setDistributorId(distributorObj.getString("distributorId"));
                            distributor.setName(distributorObj.getString("name"));
                            distributor.setPrincipalStringId(getArrayOfData.getJSONObject(i).getJSONObject("principal").getString("principalId"));
                            distributor.setPrincipalId(getArrayOfData.getJSONObject(i).getJSONObject("principal").getInt("id"));
                            distributorList.add(distributor);
                        }
                            view.updateFilteredList(distributorList);

                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    view.hideProgressBar();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.hideProgressBar();
            }
        });

    }
}
