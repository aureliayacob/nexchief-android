package com.nexchief.modul.features.distributor.edit.view;

import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;

import java.util.List;

public interface EditDistributorViewContract {
    void showPrincipals(List<Principal> principals);

    void getData(Distributor distributor);

    void onSuccess(String message);

    void onFailed(String message);
}
