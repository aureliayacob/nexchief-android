package com.nexchief.modul.features.distributor.add.presenter;

import android.content.SharedPreferences;
import android.widget.AutoCompleteTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;

public interface AddDistributorPresenterContract {

    void saveData(TextInputEditText distributorId, TextInputLayout distributorIdLyt, AutoCompleteTextView principalDropDown, TextInputEditText name, TextInputEditText address, TextInputEditText city,
                  TextInputEditText firstName, TextInputEditText lastName, TextInputEditText phone,
                  TextInputLayout dropDownLayout,
                  TextInputLayout nameLayout, TextInputLayout addressLayout,
                  TextInputLayout cityLayout, TextInputLayout firstNameLayout,
                  TextInputLayout lastNameLayout, TextInputLayout phoneLayout,
                  Distributor distributor, SharedPreferences sharedPreferences,
                  TextInputEditText email, TextInputLayout emailLayout);

    boolean anyChanges(AutoCompleteTextView principalDropDown, TextInputEditText name, TextInputEditText address, TextInputEditText city,
                       TextInputEditText firstName, TextInputEditText lastName, TextInputEditText phone);

    void getPrincipals();

    Principal getPrincipalByString(String string);


}
