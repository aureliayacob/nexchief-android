package com.nexchief.modul.features.user.detail.presenter;

import android.content.Intent;

import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.model.Distributor;
import com.nexchief.model.User;

public interface DetailUserPresenterContract {

    String getDistributor(User user);

    void getData(Intent intent);

    void deleteUser(int id);

    void isDistributorActive(TextInputLayout distributorLyt, User user);
}
