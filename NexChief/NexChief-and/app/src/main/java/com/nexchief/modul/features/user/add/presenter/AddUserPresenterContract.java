package com.nexchief.modul.features.user.add.presenter;

import android.content.SharedPreferences;
import android.widget.AutoCompleteTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.model.User;

import org.w3c.dom.Text;

public interface AddUserPresenterContract {

    void getPrincipals();

    void getDistributors(int id);

    Principal getPrincipalByString(String string);

    Distributor getDistributorByString(String string);

    void saveData(TextInputEditText email, TextInputLayout emailLyt, TextInputEditText validThru, TextInputLayout validThruLyt, AutoCompleteTextView principalDropDown, TextInputEditText userId, TextInputEditText name, TextInputEditText password,
                  TextInputEditText address, TextInputEditText phone, Principal principal,
                  TextInputLayout userIdLyt, TextInputLayout nameLyt, TextInputLayout passwordLyt,
                  TextInputLayout addressLyt, TextInputLayout phoneLyt, TextInputLayout principalLyt,
                  User user, SharedPreferences sharedPreferences);

    boolean anyChanges(AutoCompleteTextView principalDropDown, TextInputEditText userId,
                       TextInputEditText name, TextInputEditText password,
                       TextInputEditText address, TextInputEditText phone);

}
