package com.nexchief.modul.features.database.dashboard;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.nexchief.databinding.FragmentDashboardDatabaseBinding;
import com.nexchief.modul.features.database.dashboard.presenter.DatabasePresenter;
import com.nexchief.modul.features.database.dashboard.presenter.DatabasePresenterContract;
import com.nexchief.modul.features.database.dashboard.view.DatabaseViewContract;

public class DatabaseDashboardFragment extends Fragment implements DatabaseViewContract {

    private FragmentDashboardDatabaseBinding binding;
    private DatabasePresenter presenter;
    ActivityResultLauncher<Intent> getPermission;
    ProgressDialog progressDialog;

    Button backupDb, downloadDb;
    TextView textLastBackup, textBackupFile;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentDashboardDatabaseBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init();

        backupDb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPermissionGranted()) {
                    takePermission();
                } else {
                    presenter.backupDatabase();
                }
            }
        });

        downloadDb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.checkFileExistence(textBackupFile.getText().toString());
            }
        });

        return root;
    }

    private void takePermission() {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.R) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.setData(Uri.parse(String.format("package:%s", getActivity().getApplicationContext().getPackageName())));
                startActivity(intent);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.MANAGE_EXTERNAL_STORAGE}, 101);
        }

    }

    boolean isPermissionGranted() {
        //android 11
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else {
            //below
            int readExternalsStoragePermission = ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);
            int writeExternalsStoragePermission = ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return readExternalsStoragePermission == PackageManager.PERMISSION_GRANTED &&
                    writeExternalsStoragePermission == PackageManager.PERMISSION_GRANTED;
        }
    }

    @Override
    public void showProgressBar() {
        progressDialog.setTitle("Backup in progess");
        progressDialog.show();

    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();

    }

    @Override
    public void onSuccess(String lastBackup, String fileName) {
        textLastBackup.setText(lastBackup);
        textBackupFile.setText(fileName);

    }

    @Override
    public void onFailed(String message) {
        progressDialog.dismiss();
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onBackupSuccess(String lastBackup, String fileName) {
        progressDialog.dismiss();
        textLastBackup.setText(lastBackup);
        textBackupFile.setText(fileName);
        Toast.makeText(getContext(), "Backup Sucessful", Toast.LENGTH_SHORT);
    }

    @Override
    public void showDialog(String message) {
        AlertDialog alert = new AlertDialog.Builder(getContext())
                .setTitle("Backup Database")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        alert.show();

    }

    @Override
    public void startDownload() {
        DownloadManager.Request request = presenter.makeDownloadRequest(textBackupFile.getText().toString());
        DownloadManager downloadManager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        downloadManager.enqueue(request);

        Toast.makeText(getContext(), "Downloading Started...", Toast.LENGTH_SHORT).show();
    }


    public void init() {
        backupDb = binding.btnBackup;
        downloadDb = binding.btnDownload;
        presenter = new DatabasePresenter(this);

        textLastBackup = binding.tvLastBackup;
        textBackupFile = binding.tvFileName;

        progressDialog = new ProgressDialog(getContext());
        presenter.getData();
    }


}
