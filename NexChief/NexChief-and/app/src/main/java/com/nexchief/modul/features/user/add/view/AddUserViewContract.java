package com.nexchief.modul.features.user.add.view;

import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;

import java.util.List;

public interface AddUserViewContract {

    void showPrincipals(List<Principal> principalList);

    void showDistributors(List<String> distributorList);

    void onSuccess(String message);

    void onFailed(String message);

}
