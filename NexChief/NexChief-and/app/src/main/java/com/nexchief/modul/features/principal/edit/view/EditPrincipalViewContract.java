package com.nexchief.modul.features.principal.edit.view;

import com.nexchief.model.Principal;

public interface EditPrincipalViewContract {

    void getDetailPrincipal(Principal principal);

    void onSuccess(String message);

    void onFailed(String message);

}
