package com.nexchief.modul.features.user.dashboard.view;

import com.nexchief.model.User;

import java.util.List;

public interface UserDashboardViewContract {

    void showData(List<User> user);

    void onSuccess(String message);

    void onFailed(String message);

    void showProgressBar();

    void hideProgressBar();

    void updateUserList(List<User> users);

    void getTotalPage(int totalPage);

    void getSearchResult(List<User> users);

    void showEmptyDataHelper();

    void hideEmptyDataHelper();

}
