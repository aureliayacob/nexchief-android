package com.nexchief.modul.features.principal.dashboard.presenter;

import android.util.Log;

import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.principal.dashboard.view.PrincipalDashboardViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.PrincipalService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrincipalDashboardPresenter implements PrincipalDashboardPresenterContract {
    private final PrincipalDashboardViewContract view;
    private final PrincipalService principalService;
    private final DateFormatter formatter;

    public PrincipalDashboardPresenter(PrincipalDashboardViewContract view) {
        this.view = view;
        this.principalService = ServiceGenerator.createService(PrincipalService.class);
        this.formatter = new DateFormatter();

    }

    @Override
    public void getPrincipal(int size, int page) {
        view.showProgressBar();
        List<Principal> principalList = new ArrayList<>();
        principalService.getAllPrincipalPage(size, page).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject data;
                if (response.isSuccessful()) {
                    view.hideProgressBar();
                    view.hideEmptyDataHelper();
                    try {
                        data = new JSONObject(response.body().string());
                        JSONArray jArray = data.getJSONArray("data");

                        for (int i = 0; i < jArray.length(); i++) {
                            Principal principal = new Principal();
                            JSONObject temp = jArray.getJSONObject(i);
                            principal.setId(temp.getInt("id"));
                            principal.setName(temp.getString("name"));
                            principal.setPrincipalId(temp.getString("principalId"));
                            principal.setLicensedExpiryDate(formatter.formatStringToDate(temp.getString("licensedExpiryDate")));
                            principalList.add(principal);
                        }
                        if (page > 0) {
                            view.updatePrincipalList(principalList);
                        } else {
                            view.getData(principalList);
                        }

                    } catch (JSONException | IOException | ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        view.onFailure(new JSONObject(response.errorBody().string()).getString("message"));
                        view.showEmptyDataHelper();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    view.hideProgressBar();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.hideProgressBar();
                t.printStackTrace();
                if (t instanceof SocketTimeoutException) {
                    view.onFailure("Failed to connect to server");
                }
            }
        });

        principalService.countActivePrincipals().
                enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            int countedUser = 0;
                            try {
                                countedUser = new JSONObject(response.body().string()).getInt("data");
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                            view.getTotalPage((int) Math.ceil(countedUser / size));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });


    }

    @Override
    public void searchPrincipal(String keyword) {
        principalService.searchPrincipal(keyword).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject data;
                List<Principal> principalList = new ArrayList<>();
                if (response.isSuccessful()) {
                    try {
                        data = new JSONObject(response.body().string());
                        JSONArray jArray = data.getJSONArray("data");

                        for (int i = 0; i < jArray.length(); i++) {
                            Principal principal = new Principal();
                            JSONObject temp = jArray.getJSONObject(i);
                            principal.setId(temp.getInt("id"));
                            principal.setName(temp.getString("name"));
                            principal.setPrincipalId(temp.getString("principalId"));
                            principal.setLicensedExpiryDate(formatter.formatStringToDate(temp.getString("licensedExpiryDate")));
                            principalList.add(principal);
                        }
                        view.getSearchResult(principalList);

                    } catch (JSONException | ParseException | IOException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


}
