package com.nexchief.modul.features.principal.add.view;

public interface AddPrincipalViewContract {

    void onSuccess();

    void onFailed(String message);
}
