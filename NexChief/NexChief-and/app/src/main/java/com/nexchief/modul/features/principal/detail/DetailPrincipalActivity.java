package com.nexchief.modul.features.principal.detail;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.nexchief.R;
import com.nexchief.databinding.ActivityDetailPrincipalBinding;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.principal.detail.presenter.PrincipalDetailPresenter;
import com.nexchief.modul.features.principal.detail.view.PrincipalDetailViewContract;
import com.nexchief.util.DateFormatter;

public class DetailPrincipalActivity extends AppCompatActivity implements PrincipalDetailViewContract {

    private PrincipalDetailPresenter presenter;
    private TextView idPrincipal, name, address, city, country, email, phone, fax, expiryDate,
            createdAt, createdBy, updatedAt, updatedBy;

    ActivityDetailPrincipalBinding binding;

    private DateFormatter formatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.detail_principal);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);

        binding = ActivityDetailPrincipalBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        presenter = new PrincipalDetailPresenter(this);
        presenter.getData(this.getIntent());
        init();
        //get principal to delete
        if ((boolean) getIntent().getSerializableExtra("todelete") == true) {
            Principal todelete = (Principal) getIntent().getSerializableExtra("principal");
            presenter.checkIfHaveDistributor(todelete.getId());
        }


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void getDetailPrincipal(Principal principal) {
        idPrincipal.setText(principal.getPrincipalId().toUpperCase());
        name.setText(principal.getName());
        address.setText(principal.getAddress());
        city.setText(principal.getCity());
        country.setText(principal.getCountry());
        email.setText(principal.getEmail());
        phone.setText(principal.getPhone());
        fax.setText(principal.getFax());
        expiryDate.setText(formatter.formatDateToString(principal.getLicensedExpiryDate()));
        createdAt.setText(formatter.formatDateToString((principal.getCreatedAt())));
        createdBy.setText(principal.getCreatedBy());
        updatedAt.setText(formatter.formatDateToString((principal.getUpdatedAt())));
        updatedBy.setText(principal.getUpdatedBy());


    }

    @Override
    public void onSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();

    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void deleteDontHaveDistributor() {
        AlertDialog deleteDialog = new AlertDialog.Builder(this)
                .setTitle("DELETE")
                .setMessage("Are you sure to delete this principal?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Principal todelete = (Principal) getIntent().getSerializableExtra("principal");
                        presenter.deletePrincipal(todelete.getId());
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        finish();
                    }
                }).create();
        deleteDialog.show();

    }

    @Override
    public void deleteHaveDistributor() {
        //cant delete if have distributor
        AlertDialog deleteDialog = new AlertDialog.Builder(this)
                .setTitle("DELETE")
                .setMessage("Can't delete Principal, this Principal still have Distributor.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        finish();
                    }
                })
                .create();
        deleteDialog.show();

    }

    public void init() {
        idPrincipal = binding.tvIdPrincipal;
        name = binding.tvPrincipalName;
        address = binding.tvPrincipalAddress;
        city = binding.tvPrincipalCity;
        country = binding.tvPrincipalCountry;
        email = binding.tvPrincipalEmail;
        phone = binding.tvPrincipalPhone;
        fax = binding.tvPrincipalFax;
        expiryDate = binding.tvPrincipalExpirydate;
        createdAt = binding.tvPrincipalCreatedat;
        createdBy = binding.tvPrincipalCreatedby;
        updatedAt = binding.tvPrincipalUpdatedat;
        updatedBy = binding.tvPrincipalUpdatedby;

        formatter = new DateFormatter();

    }


}