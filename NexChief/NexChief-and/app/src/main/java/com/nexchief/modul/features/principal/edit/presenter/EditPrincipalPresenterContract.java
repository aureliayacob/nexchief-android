package com.nexchief.modul.features.principal.edit.presenter;

import android.content.Intent;
import android.content.SharedPreferences;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.model.Principal;

public interface EditPrincipalPresenterContract {

    void getData(Intent intent);

    void editPrincipal(TextInputEditText textName, TextInputEditText textAddress,
                  TextInputEditText textCity, TextInputEditText textEmail,
                  TextInputEditText textPhone, TextInputEditText textLicensed,
                  TextInputLayout textNameLyt, TextInputLayout textAddressLyt,
                  TextInputLayout textCityLyt, TextInputLayout textEmailLyt,
                  TextInputLayout textPhoneLyt, TextInputLayout textLicensedLyt,
                  Principal principal, SharedPreferences sharedPreferences);
}
