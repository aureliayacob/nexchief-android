package com.nexchief.util.retrofit.services;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DatabaseService {

    @GET("database/backup")
    Call<ResponseBody> getBackupDatabase();

    @GET("database/getrecord")
    Call<ResponseBody> getDatabasesRecord();

    @GET("database/checkfile/{filename}")
    Call<ResponseBody> checkFileExistence(@Path("filename") String filename);

}
