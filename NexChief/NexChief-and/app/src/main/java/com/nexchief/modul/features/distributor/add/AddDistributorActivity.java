package com.nexchief.modul.features.distributor.add;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.databinding.ActivityAddDistributorBinding;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.distributor.add.presenter.AddDistributorPresenter;
import com.nexchief.modul.features.distributor.add.view.AddDistributorViewContract;

import java.util.ArrayList;
import java.util.List;

public class AddDistributorActivity extends AppCompatActivity implements AddDistributorViewContract {
    private AddDistributorPresenter presenter;
    private ActivityAddDistributorBinding binding;
    private SharedPreferences sharedPreferences;
    private AutoCompleteTextView principalDropDown;
    private List<String> principalString = new ArrayList<>();
    private TextInputEditText textDistributorId, textName, textAddress, textCity, textCountry, textOwnerFirstName,
            textOwnerLastName, textEmail, textPhone, textFax, textWeb;

    private TextInputLayout principalLyt, textNameLyt, textAddressLyt, textCityLyt, textOwnerFirstNameLyt,
            textOwnerLastNameLyt, textPhoneLyt, textEmailLyt, distributorIdLyt;

    private Button btnSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Distributor");
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        binding = ActivityAddDistributorBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        setContentView(root);

        presenter = new AddDistributorPresenter(this);
        sharedPreferences = getSharedPreferences("nexchief", MODE_PRIVATE);

        init();
        presenter.getPrincipals();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Distributor newDistributor = new Distributor();
                Principal getPrincipal = presenter.getPrincipalByString(principalDropDown.getText().toString());
                if (getPrincipal != null) {
                    newDistributor.setPrincipalId(getPrincipal.getId());
                }
                newDistributor.setDistributorId(textDistributorId.getText().toString());
                newDistributor.setName(textName.getText().toString());
                newDistributor.setAddress(textAddress.getText().toString());
                newDistributor.setCity(textCity.getText().toString());
                newDistributor.setCountry(textCountry.getText().toString());
                newDistributor.setOwnerFirstName(textOwnerFirstName.getText().toString());
                newDistributor.setOwnerLastName(textOwnerLastName.getText().toString());
                newDistributor.setEmail(textEmail.getText().toString());
                newDistributor.setPhone(textPhone.getText().toString());
                newDistributor.setFax(textFax.getText().toString());
                newDistributor.setWebsite(textWeb.getText().toString());
                presenter.saveData(textDistributorId, distributorIdLyt, principalDropDown, textName, textAddress, textCity, textOwnerFirstName, textOwnerLastName, textPhone,
                        principalLyt, textNameLyt, textAddressLyt, textCityLyt, textOwnerFirstNameLyt, textOwnerLastNameLyt, textPhoneLyt,
                        newDistributor, sharedPreferences, textEmail, textEmailLyt);
            }
        });


    }

    @Override
    public void showPrincipals(List<Principal> principals) {
        for (Principal p : principals) {
            String toShow = p.getPrincipalId().toUpperCase() + " - " + p.getName().toUpperCase();
            this.principalString.add(toShow);
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(this, R.layout.item_dropdown_principal, principalString);
        principalDropDown.setAdapter(arrayAdapter);


    }

    @Override
    public void onSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();

    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    void init() {
        principalDropDown = binding.tvPrincipal;
        textDistributorId = binding.tvIdDistributor;
        textName = binding.tvDistributorName;
        textAddress = binding.tvDistributorAddress;
        textCity = binding.tvDistributorCity;
        textCountry = binding.tvDistributorCountry;
        textOwnerFirstName = binding.tvDistributorFirstname;
        textOwnerLastName = binding.tvDistributorLastname;
        textEmail = binding.tvDistributorEmail;
        textPhone = binding.tvDistributorPhone;
        textFax = binding.tvDistributorFax;
        textWeb = binding.tvDistributorWebsite;

        btnSave = binding.btSaveDistributor;

        textNameLyt = binding.distributorNameLayout;
        textAddressLyt = binding.distributorAddressLayout;
        textCityLyt = binding.distributorCityLayout;
        textOwnerFirstNameLyt = binding.distributorFirstnameLayout;
        textOwnerLastNameLyt = binding.distributorLastnameLayout;
        textPhoneLyt = binding.distributorPhoneLayout;
        principalLyt = binding.principalLayout;
        textEmailLyt = binding.distributorEmailLayout;
        distributorIdLyt = binding.distributorIdLayout;

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (presenter.anyChanges(principalDropDown, textName, textAddress, textCity, textOwnerFirstName, textOwnerLastName, textPhone)) {
            AlertDialog deleteDialog = new AlertDialog.Builder(this)
                    .setTitle("CONFIRMATION")
                    .setMessage("You have unsaved changes. Are you sure you want to leave this page?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AddDistributorActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).create();
            deleteDialog.show();
        } else {
            super.onBackPressed();
        }
    }

}