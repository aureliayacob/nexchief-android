package com.nexchief.modul.features.principal.edit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.databinding.ActivityEditPrincipalBinding;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.principal.add.AddPrincipalActivity;
import com.nexchief.modul.features.principal.edit.presenter.EditPrincipalPresenter;
import com.nexchief.modul.features.principal.edit.view.EditPrincipalViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.PrincipalService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class EditPrincipalActivity extends AppCompatActivity implements EditPrincipalViewContract {

    private EditPrincipalPresenter presenter;
    private SharedPreferences sharedPreferences;
    private ActivityEditPrincipalBinding binding;
    private TextInputEditText textPrincipalId, textName, textAddress, textCity, textCountry,
            textEmail, textPhone, textFax, textLicensed;
    private TextInputLayout textNameLyt, textAddressLyt, textCityLyt, textEmailLyt, textPhoneLyt, textLicensedLyt;
    private Button btnSave;
    private TextInputEditText createdAt, createdBy, updatedAt, updatedBy;
    private DateFormatter formatter;

    private Calendar calendar;

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String dateToSet = formatter.formatDateToString(calendar.getTime());

            textLicensed.setText(dateToSet);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditPrincipalBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle(R.string.edit_principal);
        setContentView(root);

        init();

        presenter = new EditPrincipalPresenter(this);
        presenter.getData(getIntent());

        textLicensed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog pickerDialog = new DatePickerDialog(EditPrincipalActivity.this,
                        R.style.MyDatePickerStyle,
                        dateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                pickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                pickerDialog.show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSave.setEnabled(false);
                Principal editPrincipal = (Principal) getIntent().getSerializableExtra("principal");
                editPrincipal.setName(textName.getText().toString());
                editPrincipal.setAddress(textAddress.getText().toString());
                editPrincipal.setCity(textCity.getText().toString());
                editPrincipal.setEmail(textEmail.getText().toString());
                editPrincipal.setPhone(textPhone.getText().toString());
                editPrincipal.setFax(textFax.getText().toString());
                editPrincipal.setCountry(textCountry.getText().toString());
                editPrincipal.setPrincipalId(textPrincipalId.getText().toString());
                editPrincipal.setUpdatedBy(sharedPreferences.getString("username", ""));
                try {
                    editPrincipal.setLicensedExpiryDate(formatter.formatStringToDate(textLicensed.getText().toString()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                presenter.editPrincipal(textName, textAddress, textCity, textEmail, textPhone, textLicensed, textNameLyt, textAddressLyt,
                        textCityLyt, textEmailLyt, textPhoneLyt, textLicensedLyt, editPrincipal, sharedPreferences);
            }
        });

    }

    @Override
    public void getDetailPrincipal(Principal principal) {
        textPrincipalId.setText(principal.getPrincipalId().toUpperCase());
        textName.setText(principal.getName());
        textAddress.setText(principal.getAddress());
        textCity.setText(principal.getCity());
        textCountry.setText(principal.getCountry());
        textEmail.setText(principal.getEmail());
        textPhone.setText(principal.getPhone());
        textFax.setText(principal.getFax());
        textLicensed.setText(formatter.formatDateToString(principal.getLicensedExpiryDate()));
        createdAt.setText(formatter.formatDateToString(principal.getCreatedAt()));
        createdBy.setText(principal.getCreatedBy());
        updatedAt.setText(formatter.formatDateToString(principal.getUpdatedAt()));
        updatedBy.setText(principal.getUpdatedBy());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        AlertDialog deleteDialog = new AlertDialog.Builder(this)
                .setTitle("CONFIRMATION")
                .setMessage("Are you sure you want cancel edit?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditPrincipalActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create();
        deleteDialog.show();
    }

    @Override
    public void onSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        btnSave.setEnabled(true);
        finish();
    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        btnSave.setEnabled(true);
    }

    public void init() {
        sharedPreferences = getSharedPreferences("nexchief", MODE_PRIVATE);
        calendar = Calendar.getInstance();

        textPrincipalId = binding.tvIdPrincipal;
        btnSave = binding.btSavePrincipal;
        textPrincipalId = binding.tvIdPrincipal;
        textName = binding.tvPrincipalName;
        textAddress = binding.tvPrincipalAddress;
        textCity = binding.tvPrincipalCity;
        textCountry = binding.tvPrincipalCountry;
        textEmail = binding.tvPrincipalEmail;
        textPhone = binding.tvPrincipalPhone;
        textFax = binding.tvPrincipalFax;
        textLicensed = binding.tvPrincipalExpirydate;
        textNameLyt = binding.principalNameLayout;
        textAddressLyt = binding.principalAddressLayout;
        textCityLyt = binding.principalCityLayout;
        textEmailLyt = binding.principalEmailLayout;
        textPhoneLyt = binding.principalPhoneLayout;
        textLicensedLyt = binding.principalDatepickerLayout;

        createdAt = binding.tvPrincipalCreatedat;
        createdBy = binding.tvPrincipalCreatedby;
        updatedAt = binding.tvPrincipalUpdatedat;
        updatedBy = binding.tvPrincipalUpdatedby;

        textLicensed.setInputType(InputType.TYPE_NULL);
        textLicensed.setKeyListener(null);

        createdAt.setKeyListener(null);
        createdBy.setKeyListener(null);
        updatedAt.setKeyListener(null);
        updatedBy.setKeyListener(null);

        formatter = new DateFormatter();

    }
}