package com.nexchief.modul.features.principal.dashboard.view;

import com.nexchief.model.Principal;

import java.util.List;

public interface PrincipalDashboardViewContract {

    void getData(List<Principal> principalList);

    void updatePrincipalList(List<Principal> principals);

    void showProgressBar();

    void hideProgressBar();

    void getTotalPage(int totalPage);

    void getSearchResult(List<Principal> principals);

    void onFailure(String message);

    void showEmptyDataHelper();

    void hideEmptyDataHelper();
}
