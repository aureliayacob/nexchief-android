package com.nexchief.modul.login.presenter;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.model.LoginCredentials;

public interface LoginPresenterContract {
    void login(LoginCredentials credentials);

    void validate(TextInputLayout usernameLayout, TextInputLayout passwordLayout,
                  TextInputEditText textUsername, TextInputEditText textPassword,
                  LoginCredentials credentials);

    void onFailed(String message);

}
