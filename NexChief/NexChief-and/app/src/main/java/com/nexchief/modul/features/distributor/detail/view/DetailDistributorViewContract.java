package com.nexchief.modul.features.distributor.detail.view;

import com.nexchief.model.Distributor;

public interface DetailDistributorViewContract {

    void getDetailDistributor(Distributor distributor);

    void onSuccess(String message);

    void onFailed(String message);

}
