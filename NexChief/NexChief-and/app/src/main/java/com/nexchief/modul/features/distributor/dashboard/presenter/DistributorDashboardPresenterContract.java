package com.nexchief.modul.features.distributor.dashboard.presenter;

public interface DistributorDashboardPresenterContract {
    void getDistributor(int size, int page);

    void searchDistributor(String keyword);
}
