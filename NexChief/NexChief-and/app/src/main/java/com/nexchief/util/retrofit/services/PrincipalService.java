package com.nexchief.util.retrofit.services;

import com.nexchief.model.Principal;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PrincipalService {

    @GET("principals/count")
    Call<ResponseBody> countActivePrincipals();

    @GET("principal/all")
    Call<ResponseBody> getAllPrincipal();

    @GET("principal/{size}/{page}")
    Call<ResponseBody> getAllPrincipalPage(@Path("size") int size, @Path("page") int page);

    @GET("principal/")
    Call<ResponseBody> getPrincipalById(@Query("id") String id);

    @POST("principal/add")
    Call<ResponseBody> addNewPrincipal(@Body Principal principal);

    @PUT("principal/edit/{id}")
    Call<ResponseBody> editPrincipal(@Path("id") int id, @Body Principal principal);

    @DELETE("principal/delete/{id}")
    Call<ResponseBody> deletePrincipal(@Path("id") int id);

    @GET("principal/search")
    Call<ResponseBody> searchPrincipal(@Query("name") String name);

    @GET("principal/{id}/havedistributor")
    Call<ResponseBody> isHaveDistributor(@Path("id") int principalId);
}
