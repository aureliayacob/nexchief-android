package com.nexchief.util.retrofit.services;

import com.nexchief.model.User;
import com.nexchief.model.LoginCredentials;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {
    @GET("users/count")
    Call<ResponseBody> countUsers();

    @GET("user/all")
    Call<ResponseBody> getAllUser();

    @GET("user/{id}")
    Call<ResponseBody> getUserById(@Path("id") int id);

    @GET("user/{size}/{page}")
    Call<ResponseBody> getAllUserPage(@Path("size") int size, @Path("page") int page);

    @POST("auth/login")
    Call<ResponseBody> adminLogin(@Body LoginCredentials payload);

    @POST("user/new")
    Call<ResponseBody> addNewUser(@Body User user);

    @PUT("user/edit/{id}")
    Call<ResponseBody> editUser(@Path("id") int id, @Body User user);

    @DELETE("user/delete/{id}")
    Call<ResponseBody> deleteUser(@Path("id") int id);

    @GET("user/search")
    Call<ResponseBody> searchUser(@Query("keyword") String keyword);


}
