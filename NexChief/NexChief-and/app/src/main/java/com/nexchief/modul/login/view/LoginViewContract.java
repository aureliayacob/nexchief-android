package com.nexchief.modul.login.view;

import com.nexchief.model.LoginCredentials;

public interface LoginViewContract {
    void showProgressBar();

    void hideProgressBar();

    void onSuccess(LoginCredentials credentials);

    void onFailed(String message);
}
