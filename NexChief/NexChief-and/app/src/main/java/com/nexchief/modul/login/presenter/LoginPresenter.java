package com.nexchief.modul.login.presenter;


import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.LoginCredentials;
import com.nexchief.modul.login.view.LoginViewContract;
import com.nexchief.util.retrofit.services.UserService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginPresenter implements LoginPresenterContract {
    private final LoginViewContract view;
    private UserService userService;

    public LoginPresenter(LoginViewContract loginView) {
        this.view = loginView;
        userService = ServiceGenerator.createService(UserService.class);
    }

    public void start(LoginCredentials credentials) {
        view.showProgressBar();
        login(credentials);
    }

    @Override
    public void login(LoginCredentials credentials) {
        userService.adminLogin(credentials).enqueue(new Callback<ResponseBody>() {
            String message;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                LoginCredentials credentials;
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject loginResponse = (JSONObject) jsonObject.get("data");
                        credentials = new LoginCredentials(loginResponse.getString("username"));
                        view.hideProgressBar();
                        view.onSuccess(credentials);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    onFailed("Wrong Username / Password");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
              t.printStackTrace();
              onFailed("Failed to connect to server");
            }
        });

    }

    @Override
    public void validate(TextInputLayout usernameLayout, TextInputLayout passwordLayout,
                         TextInputEditText textUsername, TextInputEditText textPassword,
                         LoginCredentials credentials) {
        if (textUsername.getText().toString().isEmpty()) {
            usernameLayout.setErrorEnabled(true);
            usernameLayout.setError("Username is empty");

        } else if (textPassword.getText().toString().isEmpty()) {
            passwordLayout.setErrorEnabled(true);
            passwordLayout.setError("Password is empty");
        } else {
            start(credentials);
        }

    }

    @Override
    public void onFailed(String message) {
        view.hideProgressBar();
        view.onFailed(message);
    }
}
