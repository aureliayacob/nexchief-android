package com.nexchief.modul.features.distributor.detail;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.databinding.ActivityDetailDistributorBinding;
import com.nexchief.model.Distributor;
import com.nexchief.modul.features.distributor.detail.presenter.DetailDistributorPresenter;
import com.nexchief.modul.features.distributor.detail.view.DetailDistributorViewContract;
import com.nexchief.util.DateFormatter;

public class DetailDistributorActivity extends AppCompatActivity implements DetailDistributorViewContract {
    private DetailDistributorPresenter presenter;
    private ActivityDetailDistributorBinding binding;

    private TextInputEditText textPrincipal, textDistributorId, textName, textAddress, textCity,
            textCountry, textFirstName, textLastName, textEmail, textPhone, textFax, textWebsite,
            textCreatedAt, textCreatedBy, textUpdatedAt, textUpdatedBy;
    private TextInputLayout principalLayout;
    private DateFormatter dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.detail_distributor);

        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);

        binding = ActivityDetailDistributorBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        setContentView(root);

        //get distributor to delete
        if ((boolean) getIntent().getSerializableExtra("delete")) {
            AlertDialog deleteDialog = new AlertDialog.Builder(this)
                    .setTitle("DELETE")
                    .setMessage("Are you sure to delete this Distributor?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Distributor todelete = (Distributor) getIntent().getSerializableExtra("distributor");
                            presenter.deleteDistributor(todelete.getId());
                        }
                    })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            finish();
                        }
                    }).create();
            deleteDialog.show();
        }
        init();

        presenter = new DetailDistributorPresenter(this);
        presenter.getData(this.getIntent());


    }

    @Override
    public void getDetailDistributor(Distributor distributor) {
        textPrincipal.setText(distributor.getPrincipalStringId().toUpperCase() + " - " + distributor.getPrincipalName());
        presenter.checkPrincipal(distributor.getPrincipalStringId(), principalLayout, textPrincipal);
        textDistributorId.setText(distributor.getDistributorId());
        textName.setText(distributor.getName());
        textAddress.setText(distributor.getAddress());
        textCity.setText(distributor.getCity());
        textCountry.setText(distributor.getCountry());
        textFirstName.setText(distributor.getOwnerFirstName());
        textLastName.setText(distributor.getOwnerLastName());
        textEmail.setText(distributor.getEmail());
        textPhone.setText(distributor.getPhone());
        textFax.setText(distributor.getFax());
        textWebsite.setText(distributor.getWebsite());
        textCreatedAt.setText(dateFormatter.formatDateToString(distributor.getCreatedAt()));
        textCreatedBy.setText(distributor.getCreatedBy());
        textUpdatedAt.setText(dateFormatter.formatDateToString(distributor.getUpdatedAt()));
        textUpdatedBy.setText(distributor.getUpdatedBy());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onSuccess(String message) {
        finish();

    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    void init() {
        textPrincipal = binding.tvPrincipal;
        textDistributorId = binding.tvIdDistributor;
        textName = binding.tvDistributorName;
        textAddress = binding.tvDistributorAddress;
        textCity = binding.tvDistributorCity;
        textCountry = binding.tvDistributorCountry;
        textFirstName = binding.tvDistributorFirstname;
        textLastName = binding.tvDistributorLastname;
        textEmail = binding.tvDistributorEmail;
        textPhone = binding.tvDistributorPhone;
        textFax = binding.tvDistributorFax;
        textWebsite = binding.tvDistributorWebsite;
        textCreatedAt = binding.tvDistributorCreatedat;
        textCreatedBy = binding.tvDistributorCreatedby;
        textUpdatedBy = binding.tvDistributorUpdatedby;
        textUpdatedAt = binding.tvDistributorUpdatedat;
        principalLayout = binding.principalLayout;

        dateFormatter = new DateFormatter();


    }


}