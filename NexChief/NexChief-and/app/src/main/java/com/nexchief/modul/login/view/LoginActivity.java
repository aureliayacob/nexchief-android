package com.nexchief.modul.login.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.databinding.ActivityLoginBinding;
import com.nexchief.modul.features.FeaturesActivity;
import com.nexchief.model.LoginCredentials;
import com.nexchief.modul.login.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginViewContract {

    private ProgressDialog progressDialog;
    private LoginPresenter presenter;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private TextInputLayout usernamelayout, passwordLayout;
    private TextInputEditText etUsername, etPassword;
    private ActivityLoginBinding binding;
    private Button btnLogin;
    private CheckBox checkBox;
//    String siteKey = "6LfCwL0dAAAAAEfskOk0JE1bDzsGjztdJUJjR0Jz";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        setContentView(root);

        getWindow().setStatusBarColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimaryDark));

        sharedPreferences = getSharedPreferences("nexchief", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        init();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                LoginCredentials credentials = new LoginCredentials(username, password);
                presenter.validate(usernamelayout, passwordLayout, etUsername, etPassword, credentials);
            }
        });

//        checkBox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                    recaptcha();
//
//            }
//        });


    }

    @Override
    public void showProgressBar() {
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();

    }

    @Override
    public void onSuccess(LoginCredentials credentials) {
        hideProgressBar();
        editor.putString("username", credentials.getUsername());
        editor.commit();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LoginActivity.this, FeaturesActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1000);

    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    void init() {
        progressDialog = new ProgressDialog(this);
        presenter = new LoginPresenter(this);
        etUsername = binding.etUsername;
        etPassword = binding.etPassword;
        usernamelayout = binding.tiUsernameLayout;
        passwordLayout = binding.tiPasswordLayout;
        btnLogin = binding.btnLogin;
//        checkBox = binding.checkBox;
    }

//    void recaptcha() {
//        SafetyNet.getClient(LoginActivity.this).verifyWithRecaptcha("6LfCwL0dAAAAAEfskOk0JE1bDzsGjztdJUJjR0Jz")
//                .addOnSuccessListener(new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
//                    @Override
//                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
//                        // Indicates communication with reCAPTCHA service was
//                        // successful.
//                        String userResponseToken = response.getTokenResult();
//                        if (!userResponseToken.isEmpty()) {
//                            // Validate the user response token using the
//                            // reCAPTCHA siteverify API.
//                        }
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        if (e instanceof ApiException) {
//                            // An error occurred when communicating with the
//                            // reCAPTCHA service. Refer to the status code to
//                            // handle the error appropriately.
//                            ApiException apiException = (ApiException) e;
//                            int statusCode = apiException.getStatusCode();
//                            Log.d(TAG, "Error: " + CommonStatusCodes
//                                    .getStatusCodeString(statusCode));
//                        } else {
//                            // A different, unknown type of error occurred.
//                            Log.d(TAG, "Error: " + e.getMessage());
//                        }
//                    }
//                });
//    }
}



