package com.nexchief.model;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class Principal  implements Serializable {

    private String name;
    private int id;
    private String address;
    private String city;
    private String email;
    private String phone;
    private String fax;
    private String country;
    private String principalId;
    private Date licensedExpiryDate;
    private int personId;
    private String updatedBy;
    private String createdBy;
    private Date createdAt;
    private Date updatedAt;

    private String deletedBy;

    public Principal() {
    }

    public Principal( int id, String name, String principalId) {
        this.name = name;
        this.id = id;
        this.principalId = principalId;
    }

    public Principal(String name, int id, String address, String country, String principalId, String fax, Date licensedExpiryDate, String phone, String email, String city, int personId, String updatedBy, String createdBy, Timestamp createdAt, Timestamp updatedAt) {
        this.name = name;
        this.id = id;
        this.address = address;
        this.country = country;
        this.principalId = principalId;
        this.fax = fax;
        this.licensedExpiryDate = licensedExpiryDate;
        this.phone = phone;
        this.email = email;
        this.city = city;
        this.personId = personId;
        this.updatedBy = updatedBy;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
//        this.deletedBy = deletedBy;
//        this.deletedAt = deletedAt;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPrincipalId() {
        return principalId;
    }

    public void setPrincipalId(String principalId) {
        this.principalId = principalId;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Date getLicensedExpiryDate() {
        return licensedExpiryDate;
    }

    public void setLicensedExpiryDate(Date licensedExpiryDate) {
        this.licensedExpiryDate = licensedExpiryDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }


    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }
//
//    public Timestamp getDeletedAt() {
//        return deletedAt;
//    }
//
//    public void setDeletedAt(Timestamp deletedAt) {
//        this.deletedAt = deletedAt;
//    }
}