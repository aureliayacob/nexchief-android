package com.nexchief.modul.features.principal.dashboard.presenter;

public interface PrincipalDashboardPresenterContract {
    void getPrincipal(int size, int page);
    void searchPrincipal(String keyword);

}
