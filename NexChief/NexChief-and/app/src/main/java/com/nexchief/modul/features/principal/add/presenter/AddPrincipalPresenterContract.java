package com.nexchief.modul.features.principal.add.presenter;

import android.content.SharedPreferences;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.model.Principal;

public interface AddPrincipalPresenterContract {

    void saveData(TextInputEditText textPrincipalId, TextInputLayout principalIdLyt,
                  TextInputEditText textName, TextInputEditText textAddress,
                  TextInputEditText textCity, TextInputEditText textEmail,
                  TextInputEditText textPhone, TextInputEditText textLicensed,
                  TextInputLayout textNameLyt, TextInputLayout textAddressLyt,
                  TextInputLayout textCityLyt, TextInputLayout textEmailLyt,
                  TextInputLayout textPhoneLyt, TextInputLayout textLicensedLyt,
                  Principal principal, SharedPreferences sharedPreferences);

    boolean anyChanges(TextInputEditText textPrincipalId, TextInputEditText textName,
                       TextInputEditText textAddress, TextInputEditText textCity,
                       TextInputEditText textEmail, TextInputEditText textPhone,
                       TextInputEditText textFax);
}
