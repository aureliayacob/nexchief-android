package com.nexchief.modul.features.user.detail.vew;

import com.nexchief.model.User;

public interface DetailUserViewContract {
    void getData(User user);

    void onSucces(String message);

    void onFailed(String message);

    void switchDisableLoginOn();

    void switchDisableLoginOff();
}
