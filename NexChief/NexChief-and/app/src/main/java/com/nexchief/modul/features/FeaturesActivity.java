package com.nexchief.modul.features;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.nexchief.R;

import com.nexchief.databinding.FragmentDashboardFeaturesBinding;
import com.nexchief.databinding.HeaderBinding;
import com.nexchief.modul.login.view.LoginActivity;

public class FeaturesActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private FragmentDashboardFeaturesBinding binding;
    private HeaderBinding headerBinding;
    private TextView textLoggedUser, textLogout;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = FragmentDashboardFeaturesBinding.inflate(getLayoutInflater());
        headerBinding = HeaderBinding.inflate(getLayoutInflater());

        getWindow().setStatusBarColor(ContextCompat.getColor(FeaturesActivity.this, R.color.colorWhite));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(binding.getRoot());
        setSupportActionBar(binding.appBarFeatures.toolbar);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;

        init();
        textLoggedUser.setText("Hello, " + sharedPreferences.getString("username", ""));

        textLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog logoutDialog = new AlertDialog.Builder(v.getContext())
                        .setTitle("LOGOUT")
                        .setMessage("Are you sure want to logout?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                progressDialog.show();
                                editor.clear().commit();
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (sharedPreferences.getString("username", "").equals("")) {
                                            Intent intent = new Intent(FeaturesActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            progressDialog.dismiss();
                                            finish();
                                        } else {
                                            Intent intent = new Intent(FeaturesActivity.this, FeaturesActivity.class);
                                            startActivity(intent);
                                            progressDialog.dismiss();
                                            finish();
                                        }
                                    }
                                }, 1000L);
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        }).create();
                logoutDialog.show();
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, binding.appBarFeatures.toolbar, R.string.nav_app_bar_open_drawer_description, R.string.navigation_drawer_close);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_dashboard, R.id.nav_principal, R.id.nav_distributor, R.id.nav_user, R.id.nav_database)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_features);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        if(!isNetworkAvailable()==true)
        {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.internet_alert)
                    .setMessage(R.string.check_connection)
                    .setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    }).show();
        }

    }

    public boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {


            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {

                        return true;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {

                        return true;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {

                        return true;
                    }
                }
            }
        }

        return false;

    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_features);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    void init() {
        textLoggedUser = headerBinding.tvLoggedUser;
        textLogout = binding.tvLogout;
        sharedPreferences = getSharedPreferences("nexchief", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        sharedPreferences = getSharedPreferences("nexchief", Context.MODE_PRIVATE);
        progressDialog = new ProgressDialog(this);

    }

    

}