package com.nexchief.modul.features.database.dashboard.view;

import android.app.DownloadManager;

public interface DatabaseViewContract {
    void showProgressBar();

    void hideProgressBar();

    void onSuccess(String lastBackup, String fileName);

    void onFailed(String message);

    void onBackupSuccess(String lastBackup, String fileName);

    void showDialog(String message);

    void startDownload();
}
