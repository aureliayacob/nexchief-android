package com.nexchief.util.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nexchief.R;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.distributor.detail.DetailDistributorActivity;
import com.nexchief.modul.features.distributor.edit.EditDistributorActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DistributorAdapter extends RecyclerView.Adapter<DistributorAdapter.ViewHolder> {
    private final List<Distributor> distributors;
    private List<Distributor> filteredDistributors;

    final Context context;

    public DistributorAdapter(List<Distributor> distributors, Context context) {
        this.distributors = distributors;
        this.filteredDistributors = new ArrayList<>();
        filteredDistributors.addAll(distributors);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DistributorAdapter.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_distributor_dashboard, parent, false));


    }

    @Override
    public int getItemCount() {
        return filteredDistributors.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Distributor distributor = filteredDistributors.get(position);
        holder.distributorName.setText(distributor.getName());
        holder.distributorId.setText(distributor.getDistributorId().toUpperCase());
        holder.itemView.findViewById(R.id.parent_item_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext().getApplicationContext(),
                        DetailDistributorActivity.class)
                        .putExtra("distributor", distributor)
                        .putExtra("delete", false);
                context.startActivity(intent);
            }
        });
        holder.itemView.findViewById(R.id.i_edit_distributor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext().getApplicationContext(),
                        EditDistributorActivity.class)
                        .putExtra("distributor", distributor)
                        .putExtra("toedit", distributor.getId());
                context.startActivity(intent);
            }
        });
        holder.itemView.findViewById(R.id.i_delete_distributor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext().getApplicationContext(),
                        DetailDistributorActivity.class)
                        .putExtra("distributor", distributor)
                        .putExtra("todelete", distributor.getId())
                        .putExtra("delete", true);
                context.startActivity(intent);
            }
        });


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView distributorName;
        private final TextView distributorId;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            distributorName = itemView.findViewById(R.id.tv_distributor_name);
            distributorId = itemView.findViewById(R.id.tv_distributorid);

        }
    }

    public void addToList(List<Distributor> distributors) {
        filteredDistributors.addAll(distributors);
        notifyDataSetChanged();
    }

    public void updateFilteredList(List<Distributor> distributors) {
        filteredDistributors.clear();
        filteredDistributors.addAll(distributors);
        notifyDataSetChanged();
    }

}
