package com.nexchief.modul.features.principal.edit.presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Patterns;

import androidx.annotation.RequiresApi;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.principal.edit.view.EditPrincipalViewContract;
import com.nexchief.util.DateFormatter;
import com.nexchief.util.retrofit.services.PrincipalService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPrincipalPresenter implements EditPrincipalPresenterContract {
    private final EditPrincipalViewContract view;
    private final PrincipalService principalService;
    private final DateFormatter formatter;

    public EditPrincipalPresenter(EditPrincipalViewContract view) {
        this.view = view;
        this.principalService = ServiceGenerator.createService(PrincipalService.class);
        formatter = new DateFormatter();

    }

    @Override
    public void getData(Intent intent) {
        Principal principal = (Principal) intent.getSerializableExtra("principal");
        principalService.getPrincipalById(principal.getPrincipalId()).enqueue(new Callback<ResponseBody>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject jsonObject;
                if (response.isSuccessful()) {
                    try {
                        jsonObject = new JSONObject(response.body().string());
                        JSONObject tempPrincipal = jsonObject.getJSONObject("data");
                        Principal principalData = new Principal();

                        //set data to model
                        principalData.setPrincipalId(tempPrincipal.getString("principalId"));
                        principalData.setAddress(tempPrincipal.getString("address"));
                        principalData.setName(tempPrincipal.getString("name"));
                        principalData.setCountry(tempPrincipal.getString("country"));
                        principalData.setFax(tempPrincipal.getString("fax"));
                        principalData.setLicensedExpiryDate(formatter.formatStringToDate(tempPrincipal.getString("licensedExpiryDate")));
                        principalData.setPhone(tempPrincipal.getString("phone"));
                        principalData.setEmail(tempPrincipal.getString("email"));
                        principalData.setCity(tempPrincipal.getString("city"));
                        principalData.setPersonId(tempPrincipal.getInt("personId"));
                        principalData.setUpdatedBy(tempPrincipal.getString("updatedBy"));
                        principalData.setCreatedBy(tempPrincipal.getString("createdBy"));
                        principalData.setCreatedAt(formatter.formatStringToDate(tempPrincipal.getString("createdAt")));
                        principalData.setUpdatedAt(formatter.formatStringToDate((tempPrincipal.getString("updatedAt"))));

                        view.getDetailPrincipal(principalData);
                    } catch (JSONException | IOException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    @Override
    public void editPrincipal(TextInputEditText textName, TextInputEditText textAddress,
                              TextInputEditText textCity, TextInputEditText textEmail,
                              TextInputEditText textPhone, TextInputEditText textLicensed,
                              TextInputLayout textNameLyt, TextInputLayout textAddressLyt,
                              TextInputLayout textCityLyt, TextInputLayout textEmailLyt,
                              TextInputLayout textPhoneLyt, TextInputLayout textLicensedLyt,
                              Principal principal, SharedPreferences sharedPreferences) {

        if (validate(textName, textAddress, textCity, textEmail, textPhone, textLicensed, textNameLyt,
                textAddressLyt, textCityLyt, textEmailLyt, textPhoneLyt, textLicensedLyt)) {
            principalService.editPrincipal(principal.getId(), principal).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        view.onSuccess("Edit Data Sucessfull");
                    } else {
                        try {
                            JSONObject errorObject = new JSONObject(response.errorBody().string());
                            view.onFailed(errorObject.getString("message"));
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    view.onFailed("");
                }
            });

        }


    }


    public boolean validate(TextInputEditText textName, TextInputEditText textAddress,
                            TextInputEditText textCity, TextInputEditText textEmail,
                            TextInputEditText textPhone, TextInputEditText textLicensed,
                            TextInputLayout textNameLyt, TextInputLayout textAddressLyt,
                            TextInputLayout textCityLyt, TextInputLayout textEmailLyt,
                            TextInputLayout textPhoneLyt, TextInputLayout textLicensedLyt) {
        if (textName.getText().toString().isEmpty()) {
            textNameLyt.setErrorEnabled(true);
            textNameLyt.setError("* Name is Required");
            return false;
        } else {
            textNameLyt.setErrorEnabled(false);
        }

        if (textAddress.getText().toString().isEmpty()) {
            textAddressLyt.setErrorEnabled(true);
            textAddressLyt.setError("* Address is Required");
            return false;
        } else {
            textAddressLyt.setErrorEnabled(false);
        }

        if (textAddress.getText().toString().length() > 100) {
            textAddressLyt.setErrorEnabled(true);
            textAddressLyt.setError("* Address must less than 100 characters");
            return false;
        } else {
            textAddressLyt.setErrorEnabled(false);
        }

        if (textCity.getText().toString().isEmpty()) {
            textCityLyt.setErrorEnabled(true);
            textCityLyt.setError("* City is Required");
            return false;
        } else {
            textCityLyt.setErrorEnabled(false);
        }

        if (textEmail.getText().toString().isEmpty()) {
            textEmailLyt.setErrorEnabled(true);
            textEmailLyt.setError("* Email is Required");
            return false;
        } else {
            textEmailLyt.setErrorEnabled(false);
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(textEmail.getText().toString()).matches()) {
            textEmailLyt.setErrorEnabled(true);
            textEmailLyt.setError("* Email format is invalid");
            return false;
        } else {
            textEmailLyt.setErrorEnabled(false);
        }
        if (textPhone.getText().toString().isEmpty()) {
            textPhoneLyt.setErrorEnabled(true);
            textPhoneLyt.setError("* Phone is Required");
            return false;
        } else {
            textPhoneLyt.setErrorEnabled(false);
        }

        if (textLicensed.getText().toString().equalsIgnoreCase("Select Date")) {
            textLicensedLyt.setErrorEnabled(true);
            textLicensedLyt.setError("* Select Date");
            return false;
        } else {
            textLicensedLyt.setErrorEnabled(false);
        }

        return true;
    }


}
