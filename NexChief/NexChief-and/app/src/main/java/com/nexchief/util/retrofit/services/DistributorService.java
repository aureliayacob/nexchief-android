package com.nexchief.util.retrofit.services;

import com.nexchief.model.Distributor;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DistributorService {
    @GET("distributors/count")
    Call<ResponseBody> countDistributor();

    @GET("distributor/all")
    Call<ResponseBody> getAllDistributor();

    @GET("distributor/{size}/{page}")
    Call<ResponseBody> getAllDistributorPage(@Path("size") int size, @Path("page") int page);

    @GET("distributor")
    Call<ResponseBody> getDistributorById(@Query("id") String id);

    @GET("distributors/{id}")
    Call<ResponseBody> getDistributorPerPrincipal(@Path("id") int id);

    @POST("distributor/new")
    Call<ResponseBody> addDistributor(@Body Distributor distributor);

    @PUT("distributor/edit/{id}")
    Call<ResponseBody> editDistributor(@Path("id") int id, @Body Distributor distributor);

    @DELETE("distributor/delete/{id}")
    Call<ResponseBody> deleteDistributor(@Path("id") int id);

    @GET("distributor/search")
    Call<ResponseBody> searchDistributor(@Query("name") String name);


}
