package com.nexchief.modul.features.dashboard.presenter;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexchief.generator.ServiceGenerator;
import com.nexchief.modul.features.dashboard.view.DashboardViewContract;
import com.nexchief.util.retrofit.services.DatabaseService;
import com.nexchief.util.retrofit.services.DistributorService;
import com.nexchief.util.retrofit.services.PrincipalService;
import com.nexchief.model.Principal;
import com.nexchief.util.retrofit.services.UserService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardPresenter implements DashboardPresenterContract {

    private final DashboardViewContract view;
    private final PrincipalService principalService;
    private final DistributorService distributorService;
    private final DatabaseService databaseService;
    private final UserService userService;
    private final ObjectMapper mapper;
    private int countedPrincipal, countedDistributor, countedUser;


    public DashboardPresenter(DashboardViewContract view) {
        this.view = view;
        mapper = new ObjectMapper();
        principalService = ServiceGenerator.createService(PrincipalService.class);
        distributorService = ServiceGenerator.createService(DistributorService.class);
        userService = ServiceGenerator.createService(UserService.class);
        databaseService = ServiceGenerator.createService(DatabaseService.class);
    }

    @Override
    public void getData() {
        distributorService.countDistributor().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    countedDistributor = jsonObject.getInt("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                view.countDistributor(countedDistributor);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        userService.countUsers().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    countedUser = jsonObject.getInt("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                view.countUser(countedUser);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        });

        principalService.countActivePrincipals().enqueue(new Callback<ResponseBody>() {
            List<Principal> principalList = new ArrayList<Principal>();

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        countedPrincipal = jsonObject.getInt("data");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    view.countPrincipal(countedPrincipal);

                } else {
                    view.onFailed("Failed");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


        databaseService.getDatabasesRecord().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject dataObj = new JSONObject(response.body().string()).getJSONObject("data");
                        String getFileName = dataObj.getString("fileName");
                        String[] getText = getFileName.split("_");
                        String lastBackup = getText[0];
                        view.getLastUpdated(lastBackup);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });

    }
}



