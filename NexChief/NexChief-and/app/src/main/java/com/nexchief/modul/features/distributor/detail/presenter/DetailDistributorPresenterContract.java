package com.nexchief.modul.features.distributor.detail.presenter;

import android.content.Intent;
import android.widget.AutoCompleteTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public interface DetailDistributorPresenterContract {

    void getData(Intent intent);

    void deleteDistributor(int id);

    void checkPrincipal(String id, TextInputLayout principalLayout, TextInputEditText principal);
}
