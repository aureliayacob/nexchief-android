package com.nexchief.util.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nexchief.R;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.principal.detail.DetailPrincipalActivity;
import com.nexchief.modul.features.principal.edit.EditPrincipalActivity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrincipalAdapter extends RecyclerView.Adapter<PrincipalAdapter.ViewHolder> {
    final List<Principal> principals;
    final List<Principal> allPrincipals;
    final Context context;

    public PrincipalAdapter(List<Principal> principals, Context context) {
        this.principals = principals;
        this.allPrincipals = new ArrayList<>();
        allPrincipals.addAll(principals);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PrincipalAdapter.ViewHolder(LayoutInflater.from(context).
                inflate(R.layout.item_principal_dashboard, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Principal principal = allPrincipals.get(position);
        holder.principalName.setText(principal.getName());

        if(principal.getLicensedExpiryDate().before(new Date())){
            holder.licenseStatus.setText("Expired");
            holder.licenseStatus.setTextColor(Color.parseColor("#F44336"));
        }else{
            holder.licenseStatus.setText("Active");
            holder.licenseStatus.setTextColor(Color.parseColor("#4CAF50"));
        }


        holder.itemView.findViewById(R.id.parent_item_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext().getApplicationContext(),
                        DetailPrincipalActivity.class)
                        .putExtra("principal", principal)
                        .putExtra("todelete", false);
                context.startActivity(intent);
            }
        });

        holder.itemView.findViewById(R.id.i_edit_principal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext().getApplicationContext(),
                        EditPrincipalActivity.class)
                        .putExtra("principal", principal);
                context.startActivity(intent);
            }
        });

        holder.itemView.findViewById(R.id.i_delete_principal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext().getApplicationContext(),
                        DetailPrincipalActivity.class)
                        .putExtra("principal", principal)
                        .putExtra("todelete", true);
                context.startActivity(intent);


            }
        });
    }

    @Override
    public int getItemCount() {
        return allPrincipals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView principalName;
//        private final TextView principalId;
        private final TextView licenseStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            principalName = itemView.findViewById(R.id.tv_principal_name);
//            principalId = itemView.findViewById(R.id.tv_principalid);
            licenseStatus = itemView.findViewById(R.id.tv_license_status);

        }

    }

    public void addToList(List<Principal> principals) {
        allPrincipals.addAll(principals);
        notifyDataSetChanged();
    }

    public void updateFilteredList(List<Principal> principals) {
        allPrincipals.clear();
        allPrincipals.addAll(principals);
        notifyDataSetChanged();
    }

}
