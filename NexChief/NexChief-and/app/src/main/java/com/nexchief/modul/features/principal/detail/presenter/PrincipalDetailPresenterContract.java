package com.nexchief.modul.features.principal.detail.presenter;

import android.content.Intent;

public interface PrincipalDetailPresenterContract {

    void getData(Intent intent);

    void deletePrincipal(int id);

    void checkIfHaveDistributor(int id);
}
