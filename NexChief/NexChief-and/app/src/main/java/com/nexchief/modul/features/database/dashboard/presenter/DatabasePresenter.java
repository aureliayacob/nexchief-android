package com.nexchief.modul.features.database.dashboard.presenter;

import android.app.DownloadManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.RequiresApi;

import com.nexchief.generator.ServiceGenerator;
import com.nexchief.modul.features.database.dashboard.view.DatabaseViewContract;
import com.nexchief.util.retrofit.services.DatabaseService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DatabasePresenter implements DatabasePresenterContract {
    private DatabaseViewContract view;
    private DatabaseService databaseService;


    public DatabasePresenter(DatabaseViewContract view) {
        this.view = view;
        this.databaseService = ServiceGenerator.createService(DatabaseService.class);
    }

    @Override
    public void getData() {
        databaseService.getDatabasesRecord().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject dataObj = new JSONObject(response.body().string()).getJSONObject("data");
                        String getFileName = dataObj.getString("fileName");
                        String[] getText = getFileName.split("_");
                        String lastBackup = getText[0] + " at " + getText[1];
                        view.onSuccess(lastBackup, getFileName);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    view.hideProgressBar();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.hideProgressBar();
            }
        });
    }

    @Override
    public void backupDatabase() {
        view.showProgressBar();
        databaseService.getBackupDatabase().enqueue(new Callback<ResponseBody>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String filename = new JSONObject(response.body().string()).getString("data");
                        String[] getText = filename.split("_");
                        String lastBackup = getText[0] + " " + getText[1];
                        view.onBackupSuccess(lastBackup, filename);
                        view.showDialog("Backup Successful!");
                    } catch (JSONException e) {
                        view.showDialog("Backup Failed");

                        e.printStackTrace();
                    } catch (IOException e) {
                        view.showDialog("Backup Failed");
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public DownloadManager.Request makeDownloadRequest(String fileName) {
//        String getUrl = "http://192.168.100.129:8090/api/download/" + fileName;
        String getUrl = "http://10.0.2.2:8090/api/download/" + fileName;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(getUrl));
        request.setTitle(fileName);
        request.setDescription("Nexchief Database Backup");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        return request;
    }

    @Override
    public void checkFileExistence(String filename) {
        databaseService.checkFileExistence(filename).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.startDownload();
                } else {
                    view.onFailed("File doesn't exist/already deleted. Please make a new backup.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
