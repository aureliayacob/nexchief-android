package com.nexchief.modul.features.principal.add;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.databinding.ActivityAddPrincipalBinding;
import com.nexchief.model.Principal;
import com.nexchief.modul.features.principal.add.presenter.AddPrincipalPresenter;
import com.nexchief.modul.features.principal.add.view.AddPrincipalViewContract;
import com.nexchief.util.DateFormatter;

import java.text.ParseException;
import java.util.Calendar;

public class AddPrincipalActivity extends AppCompatActivity implements AddPrincipalViewContract {
    private AddPrincipalPresenter presenter;
    private TextInputEditText textPrincipalId, textName, textAddress, textCity, textCountry,
            textEmail, textPhone, textFax, textLicensed;
    private TextInputLayout textNameLyt, textAddressLyt, textCityLyt, textEmailLyt, textPhoneLyt,
            textLicensedLyt, principalIdLyt;
    private Button btnSave;
    private ActivityAddPrincipalBinding binding;
    private DateFormatter formatter;
    private SharedPreferences sharedPreferences;

    private Calendar calendar;
    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            textLicensed.setText(formatter.formatDateToString(calendar.getTime()));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.add_principal);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);

        binding = ActivityAddPrincipalBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        init();

        textLicensed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog pickerDialog = new DatePickerDialog(AddPrincipalActivity.this,
                        R.style.MyDatePickerStyle,
                        dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                pickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                pickerDialog.show();

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSave.setEnabled(false);
                Principal newPrincipal = new Principal();
                newPrincipal.setName(textName.getText().toString());
                newPrincipal.setAddress(textAddress.getText().toString());
                newPrincipal.setCity(textCity.getText().toString());
                newPrincipal.setEmail(textEmail.getText().toString());
                newPrincipal.setPhone(textPhone.getText().toString());
                newPrincipal.setFax(textFax.getText().toString());
                newPrincipal.setCountry(textCountry.getText().toString());
                newPrincipal.setPrincipalId(textPrincipalId.getText().toString());
                try {
                    newPrincipal.setLicensedExpiryDate(formatter.formatStringToDate(textLicensed.getText().toString()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                presenter.saveData(textPrincipalId, principalIdLyt, textName, textAddress, textCity, textEmail, textPhone, textLicensed,
                        textNameLyt, textAddressLyt, textCityLyt, textEmailLyt, textPhoneLyt,
                        textLicensedLyt, newPrincipal, sharedPreferences);

            }
        });
    }

    @Override
    public void onSuccess() {
        Toast.makeText(this, "Add Data Successful!", Toast.LENGTH_SHORT).show();
        btnSave.setEnabled(true);
        finish();
    }


    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        btnSave.setEnabled(true);
    }

    @Override
    public void onBackPressed() {
        if (presenter.anyChanges(textPrincipalId, textName, textAddress, textCity, textEmail, textPhone, textFax)) {
            AlertDialog deleteDialog = new AlertDialog.Builder(this)
                    .setTitle("CONFIRMATION")
                    .setMessage("You have unsaved changes. Are you sure you want to leave this page?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AddPrincipalActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).create();
            deleteDialog.show();
        } else {
            super.onBackPressed();
        }
        btnSave.setEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void init() {
        presenter = new AddPrincipalPresenter(this);
        sharedPreferences = getSharedPreferences("nexchief", MODE_PRIVATE);
        calendar = Calendar.getInstance();
        formatter = new DateFormatter();

        btnSave = binding.btSavePrincipal;
        textPrincipalId = binding.tvIdPrincipal;
        textName = binding.tvPrincipalName;
        textAddress = binding.tvPrincipalAddress;
        textCity = binding.tvPrincipalCity;
        textCountry = binding.tvPrincipalCountry;
        textEmail = binding.tvPrincipalEmail;
        textPhone = binding.tvPrincipalPhone;
        textFax = binding.tvPrincipalFax;
        textLicensed = binding.tvPrincipalExpirydate;
        textNameLyt = binding.principalNameLayout;
        textAddressLyt = binding.principalAddressLayout;
        textCityLyt = binding.principalCityLayout;
        textEmailLyt = binding.principalEmailLayout;
        textPhoneLyt = binding.principalPhoneLayout;
        textLicensedLyt = binding.principalDatepickerLayout;
        principalIdLyt = binding.principalIdLayout;

        //uneditable but not disabled
        textLicensed.setKeyListener(null);
    }
}
