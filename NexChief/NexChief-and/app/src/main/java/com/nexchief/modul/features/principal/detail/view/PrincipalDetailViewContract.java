package com.nexchief.modul.features.principal.detail.view;

import android.content.Intent;

import com.nexchief.model.Principal;
import com.nexchief.util.adapter.PrincipalAdapter;

public interface PrincipalDetailViewContract {

    void getDetailPrincipal(Principal principal);

    void onSuccess(String message);

    void onFailed(String message);

    void deleteDontHaveDistributor();

    void deleteHaveDistributor();
}
