package com.nexchief.modul.features.user.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nexchief.R;
import com.nexchief.databinding.FragmentDashboardUserBinding;
import com.nexchief.model.User;
import com.nexchief.modul.features.user.add.AddUserActivity;
import com.nexchief.modul.features.user.dashboard.presenter.UserDashboardPresenter;
import com.nexchief.modul.features.user.dashboard.view.UserDashboardViewContract;
import com.nexchief.util.adapter.UserAdapter;

import java.util.List;

public class UserDashboardFragment extends Fragment implements UserDashboardViewContract {

    private UserDashboardPresenter presenter;
    private FragmentDashboardUserBinding binding;

    private RecyclerView recyclerView;
    private UserAdapter userAdapter;

    private SearchView searchView;
    private NestedScrollView nestedScrollView;
    private int size = 7, page = 0, totalPage = 0;
    private ProgressBar progressBar;
    private TextView tvEmptyData;


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.feature_search, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        searchView = (SearchView) item.getActionView();

        super.onCreateOptionsMenu(menu, inflater);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            //TODO -- search by API - DONE
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.searchUser(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                presenter.searchUser(newText);
                return false;
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentDashboardUserBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        setHasOptionsMenu(true);
        recyclerView = binding.recyclerViewUsers;

        presenter = new UserDashboardPresenter(this);
        nestedScrollView = binding.scrollViewUser;
        progressBar = binding.progressBar;
        tvEmptyData = binding.tvEmptyData;

        presenter.getUser(size, page);
        binding.fabAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddUserActivity.class);
                startActivity(intent);
            }
        });

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight() && page <= totalPage) {
                    Log.i("IS TRUE", "------------->" + (page <= totalPage));
                    page++;
                    presenter.getUser(size, page);
                }
            }
        });
        return root;

    }

    @Override
    public void onResume() {
        page = 0;
        presenter.getUser(size, page);
        super.onResume();
    }

    @Override
    public void showData(List<User> user) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        userAdapter = new UserAdapter(user, this.getContext());
        recyclerView.setAdapter(userAdapter);
    }

    @Override
    public void onSuccess(String message) {
    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void updateUserList(List<User> users) {
        userAdapter.addToList(users);
    }

    @Override
    public void getTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    @Override
    public void getSearchResult(List<User> users) {
        userAdapter.updateFilteredList(users);
    }

    @Override
    public void showEmptyDataHelper() {
        tvEmptyData.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyDataHelper() {
        tvEmptyData.setVisibility(View.INVISIBLE);
    }

}
