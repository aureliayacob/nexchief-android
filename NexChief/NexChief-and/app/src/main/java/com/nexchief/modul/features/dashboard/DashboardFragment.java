package com.nexchief.modul.features.dashboard;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nexchief.databinding.FragmentMainDashboardBinding;
import com.nexchief.modul.features.dashboard.presenter.DashboardPresenter;
import com.nexchief.modul.features.dashboard.view.DashboardViewContract;

public class DashboardFragment extends Fragment implements DashboardViewContract {

    private FragmentMainDashboardBinding binding;
    private DashboardPresenter presenter;
    private int countPrincipal, countDistributor, countUser;
    private TextView countPrincipalView;
    private TextView countDistributoView;
    private TextView countUserView;
    private TextView lastBackup;
    private CardView cardPrincipal;
    private CardView cardDistributor;
    private CardView cardUser;
    private CardView cardDatabase;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentMainDashboardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        countPrincipalView = binding.tvPrincipalCount;
        countDistributoView = binding.tvDistributorCount;
        countUserView = binding.tvUserCount;
        lastBackup = binding.tvDbDate;

        cardPrincipal = binding.cvPrincipal;
        cardDistributor = binding.cvDistributor;
        cardUser = binding.cvUser;
        cardDatabase = binding.cvDatabase;

        presenter = new DashboardPresenter(this);
        presenter.getData();


        return root;
    }

    @Override
    public void getData(int cPrincipal, int cDistributor, int cUser) {
    }

    @Override
    public void countPrincipal(int countPrincipal) {
        this.countPrincipal = countPrincipal;
        countPrincipalView.setText(String.valueOf(this.countPrincipal));
    }

    @Override
    public void countDistributor(int countDistributor) {
        this.countDistributor = countDistributor;
        countDistributoView.setText(String.valueOf(this.countDistributor));
    }

    @Override
    public void getLastUpdated(String lastUpdated) {
        this.lastBackup.setText(lastUpdated);

    }

    @Override
    public void countUser(int countUser) {
        this.countUser = countUser;
        countUserView.setText(String.valueOf(this.countUser));
    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this.getContext(), message, Toast.LENGTH_SHORT).show();
    }
}