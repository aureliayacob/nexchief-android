package com.nexchief.modul.features.user.add;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nexchief.R;
import com.nexchief.databinding.ActivityAddUserBinding;
import com.nexchief.model.Distributor;
import com.nexchief.model.Principal;
import com.nexchief.model.User;
import com.nexchief.modul.features.principal.add.AddPrincipalActivity;
import com.nexchief.modul.features.user.add.presenter.AddUserPresenter;
import com.nexchief.modul.features.user.add.view.AddUserViewContract;
import com.nexchief.util.DateFormatter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddUserActivity extends AppCompatActivity implements AddUserViewContract {
    private AddUserPresenter presenter;
    private SharedPreferences sharedPreferences;
    private ActivityAddUserBinding binding;
    private AutoCompleteTextView principalDropdown, distributorDropdown, statusDropDown;

    List<String> principalString;

    private TextInputEditText textUserId, textName, textAddress,
            textPhone, textEmail, textUserValidThru,
            textPassword;
    private TextInputLayout userIdLyt, principalLyt, nameLyt, passwordLyt, addressLyt, phoneLyt,
            validThruLyt, emailLyt;
    private List<String> status;

    private SwitchCompat disableLogin;
    private Button btnSave;
    private boolean userDisablelogin = false;

    private DateFormatter dateFormatter;
    Calendar calendar;
    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String dateToSet = dateFormatter.formatDateToString(calendar.getTime());
            textUserValidThru.setText(dateToSet);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddUserBinding.inflate(getLayoutInflater());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.add_user);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFFFF"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        View root = binding.getRoot();
        setContentView(root);

        sharedPreferences = getSharedPreferences("nexchief", MODE_PRIVATE);
        presenter = new AddUserPresenter(this);
        presenter.getPrincipals();

        init();

        textUserValidThru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog pickerDialog = new DatePickerDialog(
                        AddUserActivity.this,
                        R.style.MyDatePickerStyle,
                        dateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                pickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                pickerDialog.show();
            }
        });

        disableLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    userDisablelogin = true;
                }

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User newUser = new User();
                Distributor getDistributor = null;
                Principal getPrincipal = null;
                if (!principalDropdown.getText().toString().isEmpty()) {
                    getPrincipal = presenter.getPrincipalByString(principalDropdown.getText().toString());
                    newUser.setPrincipalId(getPrincipal.getId());
                }

                if (distributorDropdown.getText().toString().isEmpty()) {
                    getDistributor = null;
                } else {
                    getDistributor = presenter.getDistributorByString(distributorDropdown.getText().toString());
                }
                if (getDistributor != null) {
                    newUser.setDistributorId(getDistributor.getId());
                } else {
                    newUser.setDistributorId(null);
                }
                newUser.setUserId(textUserId.getText().toString());
                newUser.setName(textName.getText().toString());
                newUser.setAddress(textAddress.getText().toString());
                newUser.setPhone(textPhone.getText().toString());
                newUser.setEmail(textEmail.getText().toString());
                newUser.setStatus(statusDropDown.getText().toString());
                newUser.setDisableLogin(userDisablelogin);
                newUser.setPassword(textPassword.getText().toString());

                try {
                    newUser.setUserValidThru(dateFormatter.formatStringToDate(textUserValidThru.getText().toString()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                presenter.saveData(textEmail, emailLyt, textUserValidThru, validThruLyt, principalDropdown, textUserId, textName, textPassword, textAddress, textPhone,
                        getPrincipal, userIdLyt, nameLyt, passwordLyt, addressLyt, phoneLyt, principalLyt, newUser, sharedPreferences);
            }
        });

        principalDropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Principal principal = presenter.getPrincipalByString(parent.getItemAtPosition(position).toString());
                distributorDropdown.setText("");
                presenter.getDistributors(principal.getId());
            }
        });
        loadStatus();
    }

    @Override
    public void showPrincipals(List<Principal> principalList) {
        for (Principal p : principalList) {
            String toShow = p.getPrincipalId().toUpperCase() + " - " + p.getName().toUpperCase();
            principalString.add(toShow);
        }
        ArrayAdapter<String> principalAdapter = new ArrayAdapter(this, R.layout.item_dropdown_principal, principalString);
        principalDropdown.setAdapter(principalAdapter);
    }

    @Override
    public void showDistributors(List<String> distributorList) {
        ArrayAdapter<String> distributorAdapter = new ArrayAdapter(this, R.layout.item_dropdown_distributor, distributorList);
        distributorDropdown.setAdapter(distributorAdapter);
    }

    public void loadStatus() {
        ArrayAdapter<String> statusAdapter = new ArrayAdapter(this, R.layout.item_dropdown_status, status);
        statusDropDown.setText(getString(R.string.status_regular), false);
        statusDropDown.setAdapter(statusAdapter);
    }

    @Override
    public void onSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (presenter.anyChanges(principalDropdown, textUserId, textName, textPassword, textAddress, textPhone)) {
            AlertDialog deleteDialog = new AlertDialog.Builder(this)
                    .setTitle("CONFIRMATION")
                    .setMessage("You have unsaved changes. Are you sure you want to leave this page?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AddUserActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).create();
            deleteDialog.show();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    void init() {
        principalDropdown = binding.tvPrincipal;
        distributorDropdown = binding.tvDistributor;

        textUserId = binding.tvIdUser;
        textName = binding.tvUserName;
        textAddress = binding.tvUserAddress;
        textPhone = binding.tvUserPhone;
        textEmail = binding.tvUserEmail;
        textUserValidThru = binding.tvUserValidthru;
        textPassword = binding.tvUserPassword;
        disableLogin = binding.smDisableLogin;

        userIdLyt = binding.userIdLayout;
        principalLyt = binding.principalLayout;
        nameLyt = binding.userNameLayout;
        passwordLyt = binding.userPasswordLayout;
        addressLyt = binding.userAddressLayout;
        phoneLyt = binding.userPhoneLayout;
        validThruLyt = binding.userValidthruLayout;
        emailLyt = binding.userEmailLayout;

        statusDropDown = binding.userStatusDropdown;

        dateFormatter = new DateFormatter();
        calendar = Calendar.getInstance();

        principalString = new ArrayList<String>();

        btnSave = binding.btSaveUser;

        textUserValidThru.setInputType(InputType.TYPE_NULL);

        status = new ArrayList<String>();
        status.add(getString(R.string.status_premium));
        status.add(getString(R.string.status_regular));

    }
}