package com.nexchief.modul.features.database.dashboard.presenter;

import android.app.DownloadManager;

import androidx.core.content.ContextCompat;

public interface DatabasePresenterContract {
    void getData();

    void backupDatabase();

    DownloadManager.Request makeDownloadRequest(String fileName);

    void checkFileExistence(String filename);

}
